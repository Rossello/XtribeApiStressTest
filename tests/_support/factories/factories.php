<?php

use League\FactoryMuffin\Faker\Facade as Faker;

use xtribe\core\models\ad\enum\AdType;
use xtribe\core\models\ad\enum\AdCondition;
use xtribe\core\models\user\enum\UserPositionType;
use xtribe\core\models\user\enum\UserDeviceOS;
use xtribe\core\models\user\enum\UserDeviceEnvironment;

Faker::setLocale('it_IT');

$lat = ['min' => 47.094637, 'max' => 36.387515];
$lng = ['min' => 20.142263, 'max' => 5.690339];


$fm->define(xtribe\core\models\user\User::class)->setDefinitions([
    'e_mail' => Faker::email(),
    'username' => Faker::uuid(),
    'password' => Faker::password(),
    'position_type' => UserPositionType::USER_POSITION_TYPE_MOBILE,
    'lat' => Faker::latitude($lat['min'],$lat['max']),
    'lng' => Faker::longitude($lng['min'],$lng['max']),
    'date_of_birth' => Faker::date('Y-m-d','-30 years'),
    'place_of_birth' => Faker::city(),
    'state' => Faker::country(),
    'city' => Faker::city(),
    'zip_code' => Faker::postcode(),
    'address' => Faker::streetAddress(),
    'name' => Faker::firstNameMale(),
    'surname' => Faker::lastName(),
    'gender' => 'M',
    'profile_status' => Faker::sentence(5),
    'phone' => Faker::phoneNumber(),
    'default_currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
    'default_language' => 'it_IT',
    'default_timezone' => 'Europe/Rome',
    'default_country' => 'IT'
]);

$fm->define(xtribe\core\models\ad\Ad::class)->setDefinitions([
    'rif_category' => Faker::numberBetween(0, 19),
    'type' => Faker::numberBetween(AdType::AD_TYPE_PRODUCT, AdType::AD_TYPE_SERVICE),
    'name' => Faker::sentence(3),
    'description' => Faker::text(300),
    'price' => Faker::randomFloat(2,0,100),
    'conditions' => Faker::numberBetween(AdCondition::AD_CONDITION_NEW, AdCondition::AD_CONDITION_USED),
    'full_price' => Faker::randomFloat(2,100,200),
    'position_description' => Faker::text(100),
    'lat' => Faker::latitude($lat['min'],$lat['max']),
    'lng' => Faker::longitude($lng['min'],$lng['max']),
]);


$fm->define(xtribe\core\models\user\UserDevice::class)->setDefinitions([
    'device_os' => Faker::randomElement([UserDeviceOS::USER_DEVICE_ANDROID, UserDeviceOS::USER_DEVICE_IOS]),
    'device_uid' => Faker::uuid(),
    'device_token' => Faker::sha1(),
    'environment' => UserDeviceEnvironment::SANDBOX
]);


$fm->define(xtribe\core\models\appstore\AppstoreOrder::class)->setDefinitions([
    'google_transaction_id' => Faker::md5(),
    'apple_transaction_id' => Faker::md5(),
    'google_purchase_token' => Faker::sha256(),
    'apple_transaction_receipt' => Faker::sha256()
]);