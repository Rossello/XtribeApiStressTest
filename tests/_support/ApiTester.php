<?php

use xtribe\core\utils\DateTimeHandler;

use xtribe\core\models\deal\Deal;
use xtribe\core\models\deal\DealAction;

use xtribe\core\models\ad\Favorite;
use xtribe\core\models\ad\AdReport;
use xtribe\core\models\ad\AdImage;
use xtribe\core\models\ad\Ad;
use xtribe\core\models\ad\AdTag;

use xtribe\core\models\ad\elasticsearch\Ad as AdElastic;

use xtribe\core\models\xtribestore\XtribestoreOrder;

use xtribe\core\models\user\User;
use xtribe\core\models\user\UserDevice;
use xtribe\core\models\user\UserFollower;
use xtribe\core\models\gamification\UserTarget;
use xtribe\core\models\gamification\UserGoal;

use xtribe\core\models\promotion\PromotionCodeUsage;

use xtribe\core\models\tag\Tag;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    /**
     * Helper per inviare richiesta di login.
     * 
     * @param type $username
     * @param type $password
     */
    public function login($username, $password)
    {
        $I = $this;
        $I->sendPOST("/users/login", ['username' => $username, 'password' => $password]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $access_token = $I->grabDataFromResponseByJsonPath('access_token')[0];
        return $access_token;
    }
    
    /**
     * Helper per invare una richiesta di autenticazione Http.
     * 
     * @param type $username
     * @param type $password
     */
    public function authenticate($access_token) {
        $I = $this;
        $I->amHttpAuthenticated($access_token, null);
    }
    
    
    /**
     * Helper per assegnare randomaticamente un immagine
     * 
     * @return type
     */
    public function getRandomImage() {
        
        $I = $this;
        $randInt = rand(1,16);
        
        $dir = codecept_data_dir('images');
        
        $fullPath = "$dir/image_$randInt.jpg";
        
        return $fullPath;
        
    }
    
    /**
     * Registra la data di esecuzione di un test 
     * 
     * @param boolean $cleanup Controllo preliminare se esista già una data registrata
     */
    
    public function registerStartDate() {
        
        $launchDate = app\models\LaunchDate::find()->orderBy(['id' => SORT_DESC])->one();
        
        if(empty($launchDate)) {
            // Creo un nuovo record
            $launchDate = new app\models\LaunchDate();
            $launchDate->save();
        }
    }
    
}
