<?php


class ShowcaseCest
{
    public $private_id;
    public $access_token_private;
    
    
    public function _before(ApiTester $I)
    {
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->private_id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token_private = $I->login($user->username, $user->password);
    }

    public function _after(ApiTester $I)
    {
    }
    
    /**
     * Showcase geo area list
     * 
     * GET /showcase/geoareas
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function showcaseGeoareasList(ApiTester $I) {
         
        $I->wantTo("Test the call to list showcase geoareas");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the list of showcase geoareas");
        $I->sendGET("/showcase/geoareas");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of showcase geoareas with fields param");
        $I->sendGET("/showcase/geoareas",['fields' => 'id,description,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of showcase geoareas with expand param");
        $I->sendGET("/showcase/geoareas",['expand' => 'products_showcase']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of showcase geoareas with lat,lng param");
        $I->sendGET("/showcase/geoareas",['lat' => 36.453266, 'lng' => 9.133259]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
   
    /**
     * Showcase geo area list
     * 
     * GET /showcase/geoareas/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function showcaseGeoareaDetail(ApiTester $I) {
         
        $I->wantTo("Test the call to retreive the detail of a geoarea detail");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania)");
        $I->sendGET("/showcase/geoareas/301");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania) with fields param");
        $I->sendGET("/showcase/geoareas/301",['fields' => 'name,description,price']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania) with fields param");
        $I->sendGET("/showcase/geoareas/301",['fields' => 'name,description,price']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania) with expand param");
        $I->sendGET("/showcase/geoareas/301",['expand' => 'products_showcase']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Showcase geo area list
     * 
     * GET /showcase/geoareas/{$CODE}/ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function showcaseGeoareaAdsList(ApiTester $I) {
         
        $I->wantTo("Test the call to retreive the ads of a geoarea");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the ads of the area 101 (Milano)");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 101 (Milano) with fields param");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT, 'fields' => 'id,description,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 101 (Milano) with expand param");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT, 'expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 101 (Milano) with lat,lng param");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT, 'lat' => 36.453266, 'lng' => 9.133259]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
}
