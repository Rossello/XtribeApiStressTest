<?php 

class StressCest
{
    protected $user;
    protected $ad;
    
    protected $deal_id;
    protected $deal_ad_id;
    
    protected $other_user;
    protected $other_ad;

    protected $ad_to_close;

    protected $tag_id;

    public function _before(ApiTester $I)
    {
        if(empty($this->user)) {
            $output = new \Codeception\Lib\Console\Output([]);
            $I->registerStartDate();
            
            $response_user_id_A = NULL;
            //while (empty($response_user_id_A)){
                
                $user = $I->make(xtribe\core\models\user\User::class);
                $user->e_mail = uniqid().$user->e_mail;
                
                $I->amGoingTo("Save user throught api");
                $I->sendPOST("/users", [
                    'e_mail' => $user->e_mail,
                    'username' => $user->username,
                    'password' => $user->password,
                    'lat' => $user->lat,
                    'lng' => $user->lng,
                    'default_currency' => $user->default_currency,
                    'default_language' => $user->default_language,
                    'default_timezone' => $user->default_timezone,
                    'default_country' => $user->default_country
                ]);
                $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
                $I->seeResponseIsJson();
                $I->seeResponseMatchesJsonType([
                    'id' => 'integer',
                ]);
                
                $response = json_decode($I->grabResponse());
                $response_user_id_A = !empty($response->id) ? $response->id : NULL;
                
                
            //}
            
            $this->user = $user;
            $this->user->id = $response_user_id_A;
            
            $this->user->access_token = $I->login($this->user->e_mail, $this->user->password);
            
            $I->authenticate($this->user->access_token);
                
            $response_ad_id_A = NULL;
            //while (empty($response_ad_id_A)){

                $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
                    'rif_user' => $this->user->id,
                    'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_FIXED,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                ]);

                $I->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private->rif_user,
                    'rif_category' => ($ad_private->type == xtribe\core\models\ad\enum\AdCategoryType::AD_CATEGORY_TYPE_PRODUCT) ? 1 : 4,
                    'type' => $ad_private->type,
                    'name' => $ad_private->name,
                    'description' => $ad_private->description,
                    'position_type' => $ad_private->position_type,
                    'position_description' => $ad_private->position_description,
                    'lat' => $ad_private->lat,
                    'lng' => $ad_private->lng,
                    'price' => number_format($ad_private->price,2),
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private->conditions,
                    'full_price' => number_format($ad_private->full_price,2),
                    'barter' => xtribe\core\models\ad\enum\AdBarter::AD_BARTER_ON,
                    'rent' => xtribe\core\models\ad\enum\AdRent::AD_RENT_ON
                ], 
                ['image_file' => $I->getRandomImage()]
                );
                $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
                $I->seeResponseIsJson();
                $I->seeResponseMatchesJsonType([
                    'id' => 'integer',
                ]);

                $response = json_decode($I->grabResponse());
                $response_ad_id_A = !empty($response->id) ? $response->id : NULL;

            //}

            $this->ad = $ad_private;
            $this->ad->id = $response_ad_id_A;
            
            $output->writeln('response_ad_id_A: '.$response_ad_id_A);


            // ADD TO CLOSE START
            $response_ad_id_TO_CLOSE = NULL;
            //while (empty($response_ad_id_A)){

                $ad_private_to_close = $I->make(xtribe\core\models\ad\Ad::class,[
                    'rif_user' => $this->user->id,
                    'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_FIXED,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                ]);

                $I->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private_to_close->rif_user,
                    'rif_category' => ($ad_private_to_close->type == xtribe\core\models\ad\enum\AdCategoryType::AD_CATEGORY_TYPE_PRODUCT) ? 1 : 4,
                    'type' => $ad_private_to_close->type,
                    'name' => $ad_private_to_close->name,
                    'description' => $ad_private_to_close->description,
                    'position_type' => $ad_private_to_close->position_type,
                    'position_description' => $ad_private_to_close->position_description,
                    'lat' => $ad_private_to_close->lat,
                    'lng' => $ad_private_to_close->lng,
                    'price' => number_format($ad_private_to_close->price,2),
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private_to_close->conditions,
                    'full_price' => number_format($ad_private_to_close->full_price,2),
                    'barter' => xtribe\core\models\ad\enum\AdBarter::AD_BARTER_ON,
                    'rent' => xtribe\core\models\ad\enum\AdRent::AD_RENT_ON
                ], 
                ['image_file' => $I->getRandomImage()]
                );
                $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
                $I->seeResponseIsJson();
                $I->seeResponseMatchesJsonType([
                    'id' => 'integer',
                ]);

                $response = json_decode($I->grabResponse());
                $response_ad_id_TO_CLOSE = !empty($response->id) ? $response->id : NULL;

            //}

            $this->ad_to_close = $ad_private_to_close;
            $this->ad_to_close->id = $response_ad_id_TO_CLOSE;
            
            $output->writeln('response_ad_id_TO_CLOSE: '.$response_ad_id_TO_CLOSE);
            // ADD TO CLOSE END

            $response_user_id_B = NULL;
            //while (empty($response_user_id_B)){
               
                $other_user = $I->make(xtribe\core\models\user\User::class);
                $other_user->e_mail = uniqid().$other_user->e_mail;
                
                $I->sendPOST("/users", [
                    'e_mail' => $other_user->e_mail,
                    'username' => $other_user->username,
                    'password' => $other_user->password,
                    'lat' => $other_user->lat,
                    'lng' => $other_user->lng,
                    'default_currency' => $other_user->default_currency,
                    'default_language' => $other_user->default_language,
                    'default_timezone' => $other_user->default_timezone,
                    'default_country' => $other_user->default_country
                ]);
                $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
                $I->seeResponseIsJson();
                $I->seeResponseMatchesJsonType([
                    'id' => 'integer',
                ]);
                
                $response = json_decode($I->grabResponse());
                $response_user_id_B = !empty($response->id) ? $response->id : NULL;
            
            //}
            
            $this->other_user = $other_user;
            $this->other_user->id = $response_user_id_B;
            
            $this->other_user->access_token = $I->login($this->other_user->e_mail, $this->other_user->password);
            
            $I->authenticate($this->other_user->access_token);
            
            $response_ad_id_B= NULL;
            //while (empty($response_ad_id_B)){
                
                
                $other_ad = $I->make(xtribe\core\models\ad\Ad::class,[
                    'rif_user' => $this->other_user->id,
                    'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_FIXED,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                ]);
                
                $I->sendPOST("/ads", 
                    [
                        'rif_user' => $other_ad->rif_user,
                        'rif_category' => ($other_ad->type == xtribe\core\models\ad\enum\AdCategoryType::AD_CATEGORY_TYPE_PRODUCT) ? 1 : 4,
                        'type' => $other_ad->type,
                        'name' => $other_ad->name,
                        'description' => $other_ad->description,
                        'position_type' => $other_ad->position_type,
                        'position_description' => $other_ad->position_description,
                        'lat' => $other_ad->lat,
                        'lng' => $other_ad->lng,
                        'price' => number_format($other_ad->price,2),
                        'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                        'conditions' => $other_ad->conditions,
                        'full_price' => number_format($other_ad->full_price,2),
                        'barter' => xtribe\core\models\ad\enum\AdBarter::AD_BARTER_ON,
                        'rent' => xtribe\core\models\ad\enum\AdRent::AD_RENT_ON
                        
                    ], 
                    ['image_file' => $I->getRandomImage()]
                );
                $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
                $I->seeResponseIsJson();
                
                $I->seeResponseMatchesJsonType([
                    'id' => 'integer',
                ]);
                
                $response = json_decode($I->grabResponse());
                $response_ad_id_B = !empty($response->id) ? $response->id : NULL;
                
            //}
            
            $this->other_ad = $other_ad;
            $this->other_ad->id = $response_ad_id_B;
            
            $output->writeln('response_ad_id_B: '.$response_ad_id_B);
        } 
    }
    
    /**
     * ===========================
     * INIZIO TEST ENDPOINT /USERS
     * ===========================     
     */
    
    
    
    /**
     * User profile
     * 
     * GET /users/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userProfile(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive information about my user");
        $I->sendGET("/users/me");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        $I->amGoingTo("Receive information about the other_user");
        $I->sendGET("/users/".$this->other_user->id);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User profile update
     * 
     * PUT /users/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userUpdateProfile(ApiTester $I) {

        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Update my account's informations");
        $I->sendPUT('/users/me', ['name' => 'Test']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        /*$I->amGoingTo("Update other account's informations");
        $I->sendPUT('/users/'.$this->other_user->id, ['name' => $this->user->name]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();*/
        
    }
    
    
    /**
     * User register device
     * 
     * POST /users/{$ID}/devices
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userRegisterDevice(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Register one of my devices");
        $user_device = $I->make(xtribe\core\models\user\UserDevice::class,[
            'rif_user'  => $this->user->id,
            'device_os_version' => '11.1.2',
            'device_model' => 'iPhoneX',
            'client_version' => '2.1.10',
        ]);
        
        $I->sendPOST("/users/{$this->user->id}/devices",[
            'rif_user'  => $user_device->rif_user,
            'device_os' => $user_device->device_os,
            'device_os_version' => $user_device->device_os_version,
            'device_uid' => $user_device->device_uid,
            'device_token' => $user_device->device_token,
            'client_version' => $user_device->client_version,
            'device_model' => $user_device->device_model,
            'environment' => $user_device->environment
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User credit balance
     * 
     * GET /users/{$ID}/balance
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userCreditBalance(ApiTester $I) {

        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("See my account balance");
        $I->sendGET("/users/me/balance");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        $I->amGoingTo("See other user's account balance");
        $I->sendGET("/users/1/balance");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
        
    }
    
    
    /**
     * User transactions
     * 
     * GET /users/{$ID}/transactions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userTransactions(ApiTester $I) {

        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("See my transactions");
        $I->sendGET("/users/me/transactions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        $I->amGoingTo("See other user's transactions");
        $I->sendGET("/users/1/transactions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User personal ads
     * 
     * GET /users/{$ID}/ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userPersonalAds(ApiTester $I) {

        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("See my Ads");
        $I->sendGET("/users/me/ads");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Expand Category");
        $I->sendGET('/users/me/ads', ['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User favorite ads
     * 
     * GET /users/{$ID}/ads/favorites
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userFavoriteAds(ApiTester $I) {

        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("See my account's favorites ads");
        $I->sendGET("/users/me/ads/favorites");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/ads/favorites", ['expand' => 'user']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User edit favorites
     * 
     * POST     /users/{$ID}/ads/favorites
     * DELETE   /users/{$ID}/ads/favorites/{$FID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userEditFavorites(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo('Add an Ad to favorites');
        $I->sendPOST('/users/me/ads/favorites', [
            'rif_user' => $this->user->id,
            'rif_ad' => $this->ad->id
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $favorite_id = $I->grabDataFromResponseByJsonPath('id')[0];
        $I->amGoingTo('Remove an Ad to favorites');
        $I->sendDELETE("/users/me/ads/favorites/{$favorite_id}");

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
    }
    
    
    
    
    /**
     * User get followers 
     * 
     * GET /users/{$ID}/followers
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userGetFollowersUsers(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/users/me/followers");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User get followed
     * 
     * GET /users/{$ID}/followed
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userGetFollowedUsers(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/users/me/followed");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User add a followed user
     * 
     * POST /users/{$ID}/followed
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddFollowedUser(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendPOST("/users/me/followed",[
            'rif_user_follower' => $this->user->id,
            'rif_user_followed' => $this->other_user->id
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User add a followed user
     * 
     * DELETE /users/{$ID}/followed
     * 
     * @param ApiTester $I
     * @group private-users
     * @after userAddFollowedUser
     */
    public function userRemoveFollowedUser(ApiTester $I) {
    
        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/users/me/followed");
        $id_followed = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
         
        $I->sendDELETE("/users/me/followed/$id_followed");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
    }
    
    
    /**
     * User add a followed user
     * 
     * GET /users/suggested
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userSuggestedUsers(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/users/suggested");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * ===========================
     * FINE TEST ENDPOINT /USERS
     * ===========================     
     */

    
     /**
     * ===========================
     * INIZIO TEST ENDPOINT /ADS
     * ===========================     
     */
    
    /**
     * Ad list
     * 
     * GET /ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adList(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of ads in the server");
        $I->sendGET("/ads");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng fields parameter");
        $I->sendGET("/ads",['fields' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng expand parameter");
        $I->sendGET("/ads",['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng only_followed parameter");
        $I->sendGET("/ads",['only_followed' => true]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Ad detail
     * 
     * GET /ads/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adDetail(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the information of an ad in the server");
        $I->sendGET("/ads/{$this->ad->id}");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng fields paramter");
        $I->sendGET("/ads/{$this->ad->id}", ['fields' => 'name,description,pirce']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng expand paramter");
        $I->sendGET("/ads/{$this->ad->id}", ['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Ad update
     * 
     * PUT /ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adUpdate(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
       
        $I->amGoingTo("Update the Ad with id {$this->ad->id}");
        $I->sendPUT("/ads/{$this->ad->id}",[
            'rif_category' => ($this->ad->type == xtribe\core\models\ad\enum\AdCategoryType::AD_CATEGORY_TYPE_PRODUCT) ? 1 : 4,
            'description' => 'Edited Description',
            'position_type' => $this->ad->position_type,
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    
    /**
     * Ad report types
     * 
     * GET /ads/reports/types
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adReportTypes(ApiTester $I) {
       
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Retreive all the report types");
        $I->sendGET("/ads/reports/types");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    
        $I->amGoingTo("Retreive all the report types with fields params");
        $I->sendGET("/ads/reports/types",['fields' => 'code,description']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Ad report types
     * 
     * GET /ads/reports/types/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adReportTypesDetail(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/ads/reports/types");
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->sendGET("/ads/reports/types/$code");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive the report with fields params");
        $I->sendGET("/ads/reports/types/$code" ,['fields' => 'code,description']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    
    }
    
    
    /**
     * Ad insert report
     * 
     * POST /ads/reports
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adReportInsert(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Retreive a report code");
        $I->sendGET("/ads/reports/types");
        
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->sendPOST("/ads/reports", 
            [
                'rif_user' => $this->other_user->id,
                'rif_ad' => $this->ad->id,
                'rif_type' => $code,
                'message' => 'Lorem Ipsum dolor sit Amet'
                
            ]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        
    }
    
    
    /**
     * Ad search
     * 
     * GET /ads/search
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adSearch(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/ads/search", ['lat' => 45.4628328, 'lng' => 9.1076928]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'items' => [
                [
                    'type' => 'string',
                    'ad_count' => 'integer',
                    'distance' => 'string',
                    'ads' => 'array',
                    'user' => 'array',
                ]
            ]
        ]);
        
        $I->sendGET("/ads/search", [
            'lat' => 45.4628328, 
            'lng' => 9.1076928, 
            'cluster' => 'true', 
            'bound_top_left' => '45.535689,9.065118', 
            'bound_bottom_right' => '45.389779,9.290346', 
        ]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'items' => [
                [
                    'key' => 'string',
                    'doc_count' => 'integer',
                    'max_lat' => [
                        'value' => 'string'
                    ],
                    'lng' => [
                        'value' => 'string'
                    ],
                    'min_lng' => [
                        'value' => 'string'
                    ],
                    'min_lat' => [
                        'value' => 'string'
                    ],
                    'lat' => [
                        'value' => 'string'
                    ],
                    'max_lng' => [
                        'value' => 'string'
                    ]
                ]
            ]
        ]);
    }
    
    
    /**
     * Ad interesting
     * 
     * GET /ads/interesting
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adInteresting(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Get Interesting ads");
        $I->sendGET("/ads/interesting");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the categories");
        $I->sendGET("/ads/interesting",['fields' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the categories");
        $I->sendGET("/ads/interesting",['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Ad categories
     * 
     * GET /ads/categories
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adCategories(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Retreive all the categories");
        $I->sendGET("/ads/categories");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the categories passing parameters");
        $I->sendGET("/ads/categories",['fields' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    
    }
    
    
    /**
     * Ad images
     * 
     * GET /ads/{$ID}/images
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adImages(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Retreive all the images from an ad");
        $I->sendGET("/ads/{$this->ad->id}/images");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        
        $I->amGoingTo("Retreive all the images from an ad with expand fields");
        $I->sendGET("/ads/{$this->ad->id}/images",['fields' => 'id,image']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the images from an ad with expand fields");
        $I->sendGET("/ads/{$this->ad->id}/images",['expand' => 'ad']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    
    /**
     * Ad add image
     * 
     * POST /ads/{$ID}/images
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adImagesAdd(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
       
        
        $I->amGoingTo("Retreive all the images from an ad");
        $I->sendPOST("/ads/{$this->ad->id}/images",[
            'rif_ad' => $this->ad->id,
        ],
        ['image_file' => $I->getRandomImage()]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad remove image
     * 
     * DELETE /ads/{$ID}/images/{$IDI}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adImagesRemove(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendPOST("/ads/{$this->ad->id}/images",['rif_ad' => $this->ad->id],['image_file' => $I->getRandomImage()]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id_image = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Delete an image from an ad");
        $I->sendDELETE("/ads/{$this->ad->id}/images/$id_image",['rif_ad' => $this->ad->id],['image_file' => $I->getRandomImage()]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
     
    }
    
    /**
     * Ad deals
     * 
     * GET /ads/{$ID}/deals
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adDeals(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Retreive all the deals from an ad");
        $I->sendGET("/ads/{$this->ad->id}/deals");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the deals from an ad with fields param");
        $I->sendGET("/ads/{$this->ad->id}/deals", ['fields' => 'id,date_insert,status']);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the deals from an ad with expand param");
        $I->sendGET("/ads/{$this->ad->id}/deals", ['expand' => 'category']);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Ad timeline
     * 
     * GET /ads/timeline
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adTimeline(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET('/ads/timeline');
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the fields param");
        $I->sendGET('/ads/timeline',['fields' => 'id,date_insert,type']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the expand param");
        $I->sendGET('/ads/timeline',['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the max_id param");
        $I->sendGET('/ads/timeline',['max_id' => 1000]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the since_id param");
        $I->sendGET('/ads/timeline',['since_id' => 1000]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the count param");
        $I->sendGET('/ads/timeline',['count' => 100]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    
    /**
     * Ad add tag
     * 
     * POST /ads/{$ID}/tags
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adAddTags(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendPOST("/ads/{$this->ad->id}/tags", 
            [
                'rif_ad' => $this->ad->id,
                'value' => 'Prodotti',
            ]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse());
        $this->tag_id = !empty($response->id) ? $response->id : NULL; 
    }
    
    /**
     * Ad tag list
     * 
     * GET /ads/{$ID}/tags
     * 
     * @param ApiTester $I
     * @group private-users
     */
    /*public function adTagList(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/ads/{$this->ad->id}/tags");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }*/
    
    /**
     * Ad remove tag
     * 
     * DELETE /ads/{$ID}/tags/{$TID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    /*public function adRemoveTags(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);

        $I->sendDELETE("/ads/{$this->ad->id}/tags/{$this->tag_id}");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
    }*/
    
    /**
     * ===========================
     * FINE TEST ENDPOINT /ADS
     * ===========================     
     */

    
    /**
     * ===========================
     * INIZIO TEST ENDPOINT /USERS DEALS
     * ===========================     
     */
    /**
     * User deals
     * 
     * GET  /users/{$ID}/deals
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userDeals(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);

        $I->sendGET("/users/me/deals");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User open deals
     * 
     * GET  /users/{$ID}/deals/open
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userOpenDeals(ApiTester $I) {

        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/users/me/deals/open");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User closed deals
     * 
     * GET  /users/{$ID}/deals/closed
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userClosedDeals(ApiTester $I) {

        $I->authenticate($this->user->access_token);
        
        $I->sendGET("/users/me/deals/closed");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * User add a purchase proposal deal action.
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionPurchaseProposal(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PURCHASE_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->other_user->id,
            'rif_ad' => $this->other_ad->id,
            'price' => 100,
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals",['expand' => 'ad']);
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $S->authenticate($this->other_user->access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_REJECTED,
                'rif_sender_user' => $this->other_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
    }
    
    
    /**
     * User add a rental proposal deal action.
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionRentalProposal(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_RENTAL_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->other_user->id,
            'price' => 100,
            'start_date' => \xtribe\core\utils\DateTimeHandler::format(\xtribe\core\utils\DateTimeHandler::now()),
            'end_date' => \xtribe\core\utils\DateTimeHandler::format(\xtribe\core\utils\DateTimeHandler::increaseNowDateTime(7)),
        ];
        
        
        if(empty($this->deal_id)) {
            $params['rif_ad'] = $this->other_ad->id;
        } else {
            $params['rif_deal'] = $this->deal_id;
        }
         
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals");
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $S->authenticate($this->other_user->access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_REJECTED,
                'rif_sender_user' => $this->other_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
    }
    
    
    /**
     * User add a barter proposal deal action.
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionBarterProposal(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_BARTER_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->other_user->id,
            'rif_other_ad' => $this->ad->id
        ];
        
        if(empty($this->deal_id)) {
            $params['rif_ad'] = $this->other_ad->id;
        } else {
            $params['rif_deal'] = $this->deal_id;
        }
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals");
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $S->authenticate($this->other_user->access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_ACCEPTED,
                'rif_sender_user' => $this->other_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
    }
    
    
    /**
     * User add a feedback action for a deal
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionFeedback(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_BUYER_FEEDBACK,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->other_user->id,
            'feedback_value' => 1,
            'feedback_message' => 'Good Feedback Dude', 
            'rif_deal' => $this->deal_id
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $S->authenticate($this->other_user->access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_SELLER_FEEDBACK,
                'rif_sender_user' => $this->other_user->id,
                'rif_receiver_user' => $this->user->id,
                'feedback_value' => 1,
                'feedback_message' => 'Ehy Thanks, good feedback too', 
                'rif_deal' => $this->deal_id
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
    }
    
    
    /**
     * User read deal actions
     * 
     * PUT /users/{$ID}/deals/{$DID}/actions/read
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userSetDealActionsAsRead(ApiTester $I) {
     
        $I->authenticate($this->user->access_token);
        
        $I->sendPUT("/users/me/deals/$this->deal_id/actions/read");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":true}');
    }
     /**
     * ===========================
     * FINE TEST ENDPOINT /USERS DEALS
     * ===========================     
     */
    
    
    /**
     * ================================
     * INIZIO TEST ENDPOINT /PROMOTIONS
     * ================================     
     */
    
    /**
     * Promotion list
     * 
     * GET /promotions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function promotionList(ApiTester $I)  {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of promotions in the server");
        $I->sendGET("/promotions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of promotions in the server with field params");
        $I->sendGET("/promotions",['field' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of promotions in the server with field params");
        $I->sendGET("/promotions",['country_code' => 'IT']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Promotion detail
     * 
     * GET /promotions/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function promotionDetail(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of promotions in the server");
        $I->sendGET("/promotions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $promotion_id= $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->amGoingTo("Receive the id detail of the promotion in the server");
        $I->sendGET("/promotions/$promotion_id");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the promotion in the server with field params");
        $I->sendGET("/promotions/$promotion_id", ['field' => 'name,description,price']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Promotion redeem
     * 
     * GET /promotions/{$ID}/redeem
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function promotionRedeem(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Redeem the promotion with id 67");
        $I->sendPUT("/promotions/redeem", ['code' => 'VETRINA120']);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * ================================
     * FINE TEST ENDPOINT /PROMOTIONS
     * ================================     
     */
    
    /**
     * ================================
     * INIZIO TEST ENDPOINT /SHOWCASE
     * ================================     
     */
    
    /**
     * Showcase geo area list
     * 
     * GET /showcase/geoareas
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function showcaseGeoareasList(ApiTester $I) {
         
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of showcase geoareas");
        $I->sendGET("/showcase/geoareas");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of showcase geoareas with fields param");
        $I->sendGET("/showcase/geoareas",['fields' => 'id,description,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of showcase geoareas with expand param");
        $I->sendGET("/showcase/geoareas",['expand' => 'products_showcase']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of showcase geoareas with lat,lng param");
        $I->sendGET("/showcase/geoareas",['lat' => 36.453266, 'lng' => 9.133259]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Showcase geo area list
     * 
     * GET /showcase/geoareas/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function showcaseGeoareaDetail(ApiTester $I) {
         
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania)");
        $I->sendGET("/showcase/geoareas/301");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania) with fields param");
        $I->sendGET("/showcase/geoareas/301",['fields' => 'name,description,price']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania) with fields param");
        $I->sendGET("/showcase/geoareas/301",['fields' => 'name,description,price']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 301 (Catania) with expand param");
        $I->sendGET("/showcase/geoareas/301",['expand' => 'products_showcase']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    
    /**
     * Showcase geo area list
     * 
     * GET /showcase/geoareas/{$CODE}/ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function showcaseGeoareaAdsList(ApiTester $I) {
         
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the ads of the area 101 (Milano)");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 101 (Milano) with fields param");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT, 'fields' => 'id,description,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 101 (Milano) with expand param");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT, 'expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the area 101 (Milano) with lat,lng param");
        $I->sendGET("/showcase/geoareas/101/ads",['ad_type' => \xtribe\core\models\ad\enum\AdType::AD_TYPE_PRODUCT, 'lat' => 36.453266, 'lng' => 9.133259]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * ================================
     * FINE TEST ENDPOINT /SHOWCASE
     * ================================     
     */
    
    /**
     * ================================
     * INIZIO TEST PER LE NOTIFICHE
     * ================================     
     */
 
    
    /**
     * Gamification target list
     * 
     * GET /users/{$ID}/notifications
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function notification120(ApiTester $I)
    {   
        
        $I->authenticate($this->user->access_token);
        
        $user2 = $I->haveFriend('User2');
        
        $user2->does(function(ApiTester $I) {
                       
            $I->authenticate($this->other_user->access_token);
            
            $I->amGoingTo("Start following User1"); // Every move you make. 
            $I->sendPOST("/users/me/followed",[
                'rif_user_follower' => $this->other_user->id,
                'rif_user_followed' => $this->user->id
            ]);
            
            $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $I->seeResponseIsJson();
            
        });
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET('/users/me/notifications');
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $type = $I->grabDataFromResponseByJsonPath('$.items[0].type')[0];
        $I->seeResponseContainsJson(['items' => ['type' => xtribe\core\models\notification\enum\NotificationType::NEW_FOLLOWER]]);
    }
    
    
    
    /**
     * =================================
     * FINE TEST ENDPOINT /NOTIFICATIONS
     * =================================    
     */
    
    /**
     * ==================================
     * INIZIO TEST ENDPOINT /GAMIFICATION
     * ==================================     
     */
    
    /**
     * Gamification target list
     * 
     * GET /gamification/targets
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function targetList(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Get all the targets");
        $I->sendGET("/gamification/targets");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Get all the targets specifiyng the expand parameter");
        $I->sendGET("/gamification/targets", ['expand' => 'goals']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    
    /**
     * Gamification target list
     * 
     * GET /gamification/targets/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function targetDetail(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Get all the targets");
        $I->sendGET("/gamification/targets");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->amGoingTo("Get the detail of a target");
        $I->sendGET("/gamification/targets/$code");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of a target in the server specifiyng the expand parameter");
        $I->sendGET("/gamification/targets/$code", ['expand' => 'goals']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Gamification goal list
     * 
     * GET /gamification/goals
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function goalList(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Get all the goals");
        $I->sendGET("/gamification/goals");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    
    /**
     * Gamification goal detail
     * 
     * GET /gamification/goals/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function goalDetail(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Get all the goals");
        $I->sendGET("/gamification/goals");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->amGoingTo("Get the detail of a goal");
        $I->sendGET("/gamification/goals/$code");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * =================================
     * FINE TEST ENDPOINT /GAMIFICATION
     * =================================    
     */
    
    /**
     * ==================================
     * INIZIO TEST ENDPOINT /CHAT
     * ==================================     
     */
    
    /**
     * Upload an image for the chat
     * 
     * GET /showcase/geoareas
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function chatUploadImage(ApiTester $I)
    {
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Upload an image");
        $I->sendPOST("/chat/images",[],['image_file' => $I->getRandomImage()]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * =================================
     * FINE TEST ENDPOINT /CHAT
     * =================================    
     */
    
    /**
     * ==================================
     * INIZIO TEST ENDPOINT /PLAYSTORE
     * ==================================     
     */
    
    
    /**
     * Playstore list
     * 
     * GET /playstore/products
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function playstoreList(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of playstore products in the server");
        $I->sendGET("/playstore/products");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of playstore products in the server with the fields parameter");
        $I->sendGET("/playstore/products", ['fields' => 'product_id,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
   
    
    /**
     * Appstore list 
     * 
     * GET /appstore/products
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function appstoreList(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of appstore products in the server");
        $I->sendGET("/appstore/products");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of appstore products in the server with the fields parameter");
        $I->sendGET("/appstore/products", ['fields' => 'product_id,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * =================================
     * FINE TEST ENDPOINT /PLAYSTORE
     * =================================    
     */
    
    /**
     * ==================================
     * INIZIO TEST ENDPOINT /STORE
     * ==================================     
     */
    
    /**
     * Store list
     * 
     * GET /stores
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function storeList(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of stores");
        $I->sendGET("/stores");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of stores specifiyng fields parameter");
        $I->sendGET("/stores",['fields' => 'id,e_mail,date_insert']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of stores specifiyng expand parameter");
        $I->sendGET("/stores",['expand' => 'user']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
 
    
    /**
     * Store detail
     * 
     * GET /stores/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function storeDetail(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Receive the list of stores");
        $I->sendGET("/stores");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->amGoingTo("Receive the detail of store");
        $I->sendGET("/stores/$id");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * =================================
     * FINE TEST ENDPOINT /STORE
     * =================================    
     */
    
    /**
     * ==================================
     * INIZIO TEST ENDPOINT /TAGS
     * ==================================     
     */
    
    /**
     * Tag suggest
     * 
     * GET /tags/suggest
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function tagSuggest(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->sendGET('/tags/suggest',[
            'text' => 'mot'
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    /**
     * Gamification target list
     * 
     * GET /users/{$ID}/notifications/read
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function readAllNotifications(ApiTester $I) 
    {
        $I->authenticate($this->user->access_token);
        
        $I->sendPUT("/users/me/notifications/read");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['result' => TRUE]);
        
    }
    
    // CLOSING ADS
    
    /**
     * Ad close
     * 
     * PUT /ads/{$ID}/close
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adClose(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Close the Ad with id {$this->ad->id}");
        $I->sendPUT("/ads/{$this->ad_to_close->id}/close");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    // BLOCK USER
    
    /**
     * User block
     * 
     * PUT /users/{$ID}/block
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userBlockProfile(ApiTester $I) {
        
        $I->authenticate($this->user->access_token);
        
        $I->amGoingTo("Block another user");
        $I->sendPUT("/users/1/block");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();

        $I->amGoingTo("Block myself");
        $I->sendPUT("/users/me/block");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":true}');
        
    }
    
}
