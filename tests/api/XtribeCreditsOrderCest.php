<?php 

class XtribeCreditsOrderCest
{
    public $id;
    public $access_token;
    
    public function _before(ApiTester $I)
    {
            
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token = $I->login($user->username, $user->password);
        
    }

    /**
     * Playstore list
     * 
     * GET /playstore/products
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function playstoreList(ApiTester $I) {
        
        $I->wantTo("Get the playstore product list");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Receive the list of playstore products in the server");
        $I->sendGET("/playstore/products");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of playstore products in the server with the fields parameter");
        $I->sendGET("/playstore/products", ['fields' => 'product_id,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Playstore insert a product
     * 
     * POST /playstore/orders
     *
     * -- NOTE --
     * 
     * This test will actually fail because i can't generate a valid 
     * google_transaction_id token and a valid google_purchase_token
     * 
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function playstoreInsertProduct(ApiTester $I) {
        
        $I->wantTo("Get the playstore product list");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Insert a playstore product in the server");
        $I->sendGET("/playstore/products");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        
        $product_id = $I->grabDataFromResponseByJsonPath('$.items[0].product_id')[0];
        $appstoreOrder = $I->make(xtribe\core\models\appstore\AppstoreOrder::class);
        
        $I->sendPOST("/playstore/orders", 
            [
                'rif_user' => $this->id,
                'google_product_id' => $product_id,
                'google_transaction_id' => $appstoreOrder->google_transaction_id,
                'google_purchase_token' => $appstoreOrder->google_purchase_token
            ]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Appstore list 
     * 
     * GET /appstore/products
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function appstoreList(ApiTester $I) {
        
        $I->wantTo("Get the appstore product list");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Receive the list of appstore products in the server");
        $I->sendGET("/appstore/products");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of appstore products in the server with the fields parameter");
        $I->sendGET("/appstore/products", ['fields' => 'product_id,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Appstore insert product 
     * 
     * POST /appstore/orders
     * 
     * -- NOTE --
     * 
     * This test will actually fail because i can't generate a valid 
     * apple_transaction_id token and a valid apple_transaction_receipt
     * 
     * @param ApiTester $I
     */
    public function appstoreInsertProduct(ApiTester $I) {
        
        $I->wantTo("Get the appstore product list");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Insert an appstore product in the server");
        $I->sendGET("/playstore/products");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        
        $product_id = $I->grabDataFromResponseByJsonPath('$.items[0].product_id')[0];
        $appstoreOrder = $I->make(xtribe\core\models\appstore\AppstoreOrder::class);
        
        $I->sendPOST("/appstore/orders", 
            [
                'rif_user' => $this->id,
                'apple_product_id' => $product_id,
                'apple_transaction_id' => $appstoreOrder->apple_transaction_id,
                'apple_transaction_receipt' => $appstoreOrder->apple_transaction_receipt
            ]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
    }
    
}
