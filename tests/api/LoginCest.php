<?php


class LoginCest
{
    public $user;
    
    public $access_token;

    public function _before(ApiTester $I)
    {
            
        $user = $I->make(xtribe\core\models\user\User::class);

        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->user = $user;
        
    }

    public function _after(ApiTester $I)
    {
        
    }

    /**
     * Login in the application 
     * 
     * POST /users/login
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function login(ApiTester $I) {
        
        $I->wantTo("Test a correct user Login");
        $I->sendPOST("/users/login", ['username' => $this->user->username, 'password' => $this->user->password]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'user_id' => 'integer',
            'account_type' => 'integer',
            'access_token' => 'string',
        ]);
    }
    
    /**
     * Login with an invalid username
     * 
     * POST /users/login
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function loginWithIncorrectUsername(ApiTester $I) {
        
        $I->wantTo("Test a login with an incorrect username");
        $I->sendPOST("/users/login", ['username' => 'invalid_username', 'password' => $this->user->password]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'status' => 401,
            'code' => 1003,
            'moreInfo' => "http://api.xtribe.com/docs/errors/1003",
            'detail' =>  "Invalid username."
        ]);
    }
    
    /**
     * Login with an invalid password
     * 
     * POST /users/login
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function loginWithIncorrectPassword(ApiTester $I) {
        
        $I->wantTo("Test a login with an incorrect password");
        $I->sendPOST("/users/login", ['username' => $this->user->username, 'password' => 'invalid_password']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'status' => 401,
            'code' => 1005,
            'moreInfo' => "http://api.xtribe.com/docs/errors/1005",
            'detail' =>  "Invalid password."
        ]);
    }
    
    /**
     * Logout from the application
     * 
     * GET /users/logout
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userLogout(ApiTester $I) {  
        
        $I->wantTo("Test a correct user logout");
        $this->access_token = $I->login($this->user->username, $this->user->password);
        $I->authenticate($this->access_token);
        $I->sendGET("/users/logout");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseContains('{"result":true}');
        
    }
}
