<?php 

class StoreCest
{
    
    public $access_token;
    
    public $store_id;


    public function _before(ApiTester $I)
    {
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->private_id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token = $I->login($user->username, $user->password);
        
    }
    
    /**
     * Store list
     * 
     * GET /stores
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function getStoreList(ApiTester $I) {
        
        $I->wantTo("Test the call to store list node");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Receive the list of stores");
        $I->sendGET("/stores");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of stores specifiyng fields parameter");
        $I->sendGET("/stores",['fields' => 'id,e_mail,date_insert']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of stores specifiyng expand parameter");
        $I->sendGET("/stores",['expand' => 'user']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
 
    /**
     * Store detail
     * 
     * GET /stores/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function getStoreDetail(ApiTester $I) {
        
        $I->wantTo("Test the call to store list node");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Receive the list of stores");
        $I->sendGET("/stores");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->amGoingTo("Receive the detail of store");
        $I->sendGET("/stores/$id");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
}
