<?php

class AdCest
{
    public $store_id;
    public $access_token_store;
    
    public $private_id;
    public $access_token_private;
    
    
    public function _before(ApiTester $I)
    {
        if(empty($this->access_token_store) ) {           
            $this->access_token_store = $I->login('mugshop', 123);
            $this->store_id = $I->grabDataFromResponseByJsonPath('user_id')[0];
        }
        
        
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->private_id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token_private = $I->login($user->username, $user->password);
            
        
    }
    
    /**
     * Ad list
     * 
     * GET /ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adList(ApiTester $I) {
        
        $I->wantTo("Test the call to ads list node");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the list of ads in the server");
        $I->sendGET("/ads");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng fields parameter");
        $I->sendGET("/ads",['fields' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng expand parameter");
        $I->sendGET("/ads",['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng only_followed parameter");
        $I->sendGET("/ads",['only_followed' => true]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad detail
     * 
     * GET /ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adDetail(ApiTester $I) {
        
        $I->wantTo("Test the call for retreiving the detail of an Ad");
        $I->authenticate($this->access_token_private);
        
        $I->sendGET("/ads");
        $id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->amGoingTo("Receive the information of an ad in the server");
        $I->sendGET("/ads/$id");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng fields paramter");
        $I->sendGET("/ads/$id", ['fields' => 'name,description,pirce']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of ads in the server specifiyng expand paramter");
        $I->sendGET("/ads/$id", ['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad creation for private user
     * 
     * POST /ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adCreationForPrivateUser(ApiTester $I) {
        
        $I->wantTo("Test the call to create an Ad for a private user");
        
        $I->amGoingTo("Create an Ad as a private user with position type 'Mobile'");
        $I->authenticate($this->access_token_private);
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Create an Ad as a private user with position type 'Fixed'");
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_FIXED,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price,
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        
        $I->expectTo("See test failing beacuse I can't create an ad with position 'Fixed' and without position description"); 
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseIsJson();
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'position_description' => 'test',
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price,
                'lat' => '45.4628328',
                'lng' => '9.1076928',
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        
        $I->expectTo("See the ad begin created"); 
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad creation for store user
     * 
     * POST /ads
     * 
     * @param ApiTester $I
     * @group store-users
     */
    public function adCreationForStoreUser(ApiTester $I) {
        
        $I->wantTo("Test the call to create an Ad for a private user");
        
        $I->amGoingTo("Create an Ad as a store user");
        $I->authenticate($this->access_token_store);
        $ad_store = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->store_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_store->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_store->type])->id,
                'type' => $ad_store->type,
                'name' => $ad_store->name,
                'description' => $ad_store->description,
                'position_type' => $ad_store->position_type,
                'price' => $ad_store->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_store->conditions,
                'full_price' => $ad_store->full_price,
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        
        $I->amGoingTo("Create an Ad as a store user specifying the expiring date");
        $ad_store = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->store_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            'date_expiry' => xtribe\core\utils\DateTimeHandler::format(xtribe\core\utils\DateTimeHandler::increaseNowDateTime(6)),
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_store->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_store->type])->id,
                'type' => $ad_store->type,
                'name' => $ad_store->name,
                'description' => $ad_store->description,
                'position_type' => $ad_store->position_type,
                'price' => $ad_store->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_store->conditions,
                'full_price' => $ad_store->full_price,
                'date_expiry' => $ad_store->date_expiry
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->seeResponseContainsJson(['date_expiry' => $ad_store->date_expiry]);
    }
    
    /**
     * Ad creation for other user
     * 
     * POST /ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adCreationForOtherUser(ApiTester $I) {
        
        $I->wantTo("Test that i can't create an ad for another user");
        $I->authenticate($this->access_token_private);
        
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $this->store_id,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
        
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_FIXED,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $this->store_id,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Ad update
     * 
     * PUT /ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adUpdate(ApiTester $I) {
        
        $I->wantTo("Test that i can update an ad");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Create a dummy Ad for updating");
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive the last Ad I've created");
        $I->sendGET("/users/me/ads");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $ad = $I->grabDataFromResponseByJsonPath('$.items[0]')[0];
        $I->amGoingTo("Update the Ad with id {$ad['id']}");
        $I->sendPUT("/ads/{$ad['id']}",[
            'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
            'description' => 'Edited Description',
            'position_type' => $ad['position_type'],
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad close
     * 
     * PUT /ads/{$ID}/close
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adClose(ApiTester $I) {
        
        $I->wantTo("Test that i can close an ad");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Create a dummy Ad for closing");
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive the last Ad I've created");
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        
        $I->amGoingTo("Close the Ad with id {$id}");
        $I->sendPUT("/ads/{$id}/close");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad close for other user
     * 
     * PUT /ads/{$ID}/close
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adCloseForOtherUser(ApiTester $I) {
        
        $I->wantTo("Test that i can't close an ad for another user");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Create a dummy Ad for closing");
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        //$I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        //$I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive the last Ad I've created");
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $access_token = $I->login($user->username, $user->password);
        
        $I->amGoingTo("Close the Ad with id {$id}");
        $I->authenticate($access_token);
        $I->sendPUT("/ads/{$id}/close");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad renew 
     * 
     * PUT /ads/{$ID}/renew
     * 
     * @param ApiTester $I
     * @group store-users
     */
    public function adRenew(ApiTester $I) {
        
        $I->wantTo("Test that i can renew an Ad for a store user");
        $I->authenticate($this->access_token_store);
        
        $I->amGoingTo("Create a dummy Ad for closing");
        $ad_store = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->store_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            
        ]);
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_store->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_store->type])->id,
                'type' => $ad_store->type,
                'name' => $ad_store->name,
                'description' => $ad_store->description,
                'position_type' => $ad_store->position_type,
                'price' => $ad_store->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_store->conditions,
                'full_price' => $ad_store->full_price,
                'date_expiry' => xtribe\core\utils\DateTimeHandler::format(xtribe\core\utils\DateTimeHandler::increaseNowDateTime(6)),
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive the last Ad I've created");
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Renew the Ad with id {$id}");
        $I->sendPUT("/ads/{$id}/renew", ['duration' => 7]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":true}');
        
        $I->amGoingTo("Test if this action is forbidden for a private user");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Create a dummy Ad for closing");
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price,
                'date_expiry' => xtribe\core\utils\DateTimeHandler::format(xtribe\core\utils\DateTimeHandler::increaseNowDateTime(6)),
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive the last Ad I've created");
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Renew the Ad with id {$id}");
        $I->sendPUT("/ads/{$id}/renew", ['duration' => 7]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad renew 
     * 
     * PUT /ads/renew
     * 
     * @param ApiTester $I
     * @group store-users
     */
    public function adRenewAll(ApiTester $I) {
        
        $I->wantTo("Test that i can renew all Ad for a store user");
        $I->authenticate($this->access_token_store);
        
        $I->amGoingTo("Renew all the ad for the user");
        $I->sendPUT("/ads/renew", ['rif_user' => $this->store_id]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":true}');
        
        $I->amGoingTo("Test if this action is forbidden for a private user");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Renew all the ad for the user");
        $I->sendPUT("/ads/renew", ['rif_user' => $this->private_id]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Ad report types
     * 
     * GET /ads/reports/types
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adReportTypes(ApiTester $I) {
        
        $I->wantTo("Test the call for retreive the report types");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Retreive all the report types");
        $I->sendGET("/ads/reports/types");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    
        $I->amGoingTo("Retreive all the report types with fields params");
        $I->sendGET("/ads/reports/types",['fields' => 'code,description']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad report types
     * 
     * GET /ads/reports/types/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adReportTypesDetail(ApiTester $I) {
        
        $I->wantTo("Test the call for retreive a report detail");
        $I->authenticate($this->access_token_private);
        
        $I->sendGET("/ads/reports/types");
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->sendGET("/ads/reports/types/$code");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive the report with fields params");
        $I->sendGET("/ads/reports/types/$code" ,['fields' => 'code,description']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad insert report
     * 
     * POST /ads/reports
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adReportInsert (ApiTester $I) {
        
        $I->wantTo("Test the call for retreive the report types with params");
        $I->authenticate($this->access_token_private);
        
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive last ad inserted");
        $ad_private->id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Retreive a report code");
        $I->sendGET("/ads/reports/types");
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->sendPOST("/ads/reports", 
            [
                'rif_user' => $this->store_id,
                'rif_ad' => $ad_private->id,
                'rif_type' => $code,
                'message' => 'Lorem Ipsum dolor sit Amet'
                
            ]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        
    }
    
    /**
     * Ad search
     * 
     * GET /ads/search
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adSearch(ApiTester $I)
    {
        $I->wantTo("Test the ad search endpoint");
        $I->authenticate($this->access_token_private);
        $I->sendGET("/ads/search", ['lat' => 45.4628328, 'lng' => 9.1076928]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'items' => [
                [
                    'type' => 'string',
                    'ad_count' => 'integer',
                    'distance' => 'string',
                    'ads' => 'array',
                    'user' => 'array',
                ]
            ]
        ]);
        
        $I->wantTo("Test the ad search endpoint");
        $I->sendGET("/ads/search", [
            'lat' => 45.4628328, 
            'lng' => 9.1076928, 
            'cluster' => 'true', 
            'bound_top_left' => '45.535689,9.065118', 
            'bound_bottom_right' => '45.389779,9.290346', 
        ]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'items' => [
                [
                    'key' => 'string',
                    'doc_count' => 'integer',
                    'max_lat' => [
                        'value' => 'string'
                    ],
                    'lng' => [
                        'value' => 'string'
                    ],
                    'min_lng' => [
                        'value' => 'string'
                    ],
                    'min_lat' => [
                        'value' => 'string'
                    ],
                    'lat' => [
                        'value' => 'string'
                    ],
                    'max_lng' => [
                        'value' => 'string'
                    ]
                ]
            ]
        ]);
    }
    
    /**
     * Ad interesting
     * 
     * GET /ads/interesting
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adInteresting(ApiTester $I) {
        
        $I->wantTo("Test the call for interesting ads");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Get Interesting ads");
        $I->sendGET("/ads/interesting");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the categories");
        $I->sendGET("/ads/interesting",['fields' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the categories");
        $I->sendGET("/ads/interesting",['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
   
    /**
     * Ad categories
     * 
     * GET /ads/categories
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adCategories(ApiTester $I) {
        
        $I->wantTo("Test the call for retreive the categories");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Retreive all the categories");
        $I->sendGET("/ads/categories");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the categories passing parameters");
        $I->sendGET("/ads/categories",['fields' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    
    }
    
    /**
     * Ad images
     * 
     * GET /ads/{$ID}/images
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adImages(ApiTester $I) {
        
        $I->wantTo("Test the call for retreive the images of an ad");
        $I->authenticate($this->access_token_private);
        
        $I->sendGET("/ads");
        $id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->amGoingTo("Retreive all the images from an ad");
        $I->sendGET("/ads/$id/images");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        
        $I->amGoingTo("Retreive all the images from an ad with expand fields");
        $I->sendGET("/ads/$id/images",['fields' => 'id,image']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the images from an ad with expand fields");
        $I->sendGET("/ads/$id/images",['expand' => 'ad']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Ad add image
     * 
     * POST /ads/{$ID}/images
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adImagesAdd(ApiTester $I) {
        
        $I->wantTo("Test the call for adding images to an ad");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Create a dummy Ad for adding an image");
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Retreive all the images from an ad");
        $I->sendPOST("/ads/$id/images",[
            'rif_ad' => $id,
        ],
        ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad remove image
     * 
     * DELETE /ads/{$ID}/images/{$IDI}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adImagesRemove(ApiTester $I) {
        
        $I->wantTo("Test the call for remove images to an ad");
        $I->authenticate($this->access_token_private);
        
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id_ad = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Retreive all the images from an ad");
        $I->sendPOST("/ads/$id_ad/images",['rif_ad' => $id_ad],['image_file' => codecept_data_dir('logo-240x69.png')]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id_image = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Delete an image from an ad");
        $I->sendDELETE("/ads/$id_ad/images/$id_image",['rif_ad' => $id_ad],['image_file' => codecept_data_dir('logo-240x69.png')]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
     
    }
    
    /**
     * Ad deals
     * 
     * GET /ads/{$ID}/deals
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adDeals(ApiTester $I) {
        
        $I->wantTo("Test the call for retreive the deals of an ad");
        $I->authenticate($this->access_token_private);
        
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
        
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        
        $I->amGoingTo("Retreive all the deals from an ad");
        $I->sendGET("/ads/$id/deals");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the deals from an ad with fields param");
        $I->sendGET("/ads/$id/deals", ['fields' => 'id,date_insert,status']);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive all the deals from an ad with expand param");
        $I->sendGET("/ads/$id/deals", ['expand' => 'category']);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad timeline
     * 
     * GET /ads/timeline
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adTimeline(ApiTester $I) {
        
        $I->wantTo("Test the call for retreive the timeline of the ads");
        $I->authenticate($this->access_token_private);
        
        $I->sendGET('/ads/timeline');
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the fields param");
        $I->sendGET('/ads/timeline',['fields' => 'id,date_insert,type']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the expand param");
        $I->sendGET('/ads/timeline',['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the max_id param");
        $I->sendGET('/ads/timeline',['max_id' => 1000]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the since_id param");
        $I->sendGET('/ads/timeline',['since_id' => 1000]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Test the call for retreive the timeline of the ads with the count param");
        $I->sendGET('/ads/timeline',['count' => 100]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Ad add tag
     * 
     * POST /ads/{$ID}/tags
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adAddTags(ApiTester $I) {
        
        $I->wantTo("Test the call for add a tag to the ad");
        $I->authenticate($this->access_token_private);
        
        $I->sendGET("/users/me/ads");
        $ads = $I->grabDataFromResponseByJsonPath('$.items')[0];
        
        if(empty($ads)) {
            
            $ad = $I->make(xtribe\core\models\ad\Ad::class,[
                'rif_user' => $this->private_id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            ]);

            $I->sendPOST("/ads", 
                [
                    'rif_user' => $ad->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad->type])->id,
                    'type' => $ad->type,
                    'name' => $ad->name,
                    'description' => $ad->description,
                    'position_type' => $ad->position_type,
                    'price' => $ad->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad->conditions,
                    'full_price' => $ad->full_price
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );

            $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $I->seeResponseIsJson();

            $I->amGoingTo("Retreive last ad inserted");
            $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
            
        } else {
        
            $id = $ads[0]['id'];
        }
        
        $I->sendPOST("/ads/$id/tags", 
            [
                'rif_ad' => $id,
                'value' => 'Prodotti',
            ]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * Ad remove tag
     * 
     * DELETE /ads/{$ID}/tags/{$TID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adRemoveTags(ApiTester $I) {
        
        $I->wantTo("Test the call for delete a tag from an ad");
        $I->authenticate($this->access_token_private);
        
        $ad_private = $I->make(xtribe\core\models\ad\Ad::class,[
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);
         
        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad_private->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                'type' => $ad_private->type,
                'name' => $ad_private->name,
                'description' => $ad_private->description,
                'position_type' => $ad_private->position_type,
                'price' => $ad_private->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad_private->conditions,
                'full_price' => $ad_private->full_price
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Retreive last ad inserted");
        $ad_private->id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->sendPOST("/ads/$ad_private->id/tags", 
            [
                'rif_ad' => $ad_private->id,
                'value' => 'Test',
            ]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        $tagId = $I->grabDataFromResponseByJsonPath('id')[0];
        
        
        $I->sendDELETE("/ads/$ad_private->id/tags/$tagId");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
    }
    
    /**
     * Ad tag list
     * 
     * GET /ads/{$ID}/tags
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function adTagList(ApiTester $I) {
        
        $I->wantTo("Test the call for retreive the list of tag of an ad");
        $I->authenticate($this->access_token_private);
        
        $I->sendGET("/ads");
        $id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->sendGET("/ads/$id/tags");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
}
