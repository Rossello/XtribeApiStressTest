<?php 

class CleanupCest
{
    public function _before(ApiTester $I)
    {
    }

    
    public function cleanUp(ApiTester $I)
    {
        $launchDate = app\models\LaunchDate::find()->orderBy(['id' => SORT_DESC])->one();
        
        if(!empty($launchDate)) {
            
            $I->authenticate("c467602c7c59810c4560fda69c96963ce21fb75d"); // TEST API
            
            $I->sendPUT("/maintenance/clean?date={$launchDate->date}");
            $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
            $I->seeResponseContainsJson(array('result' => true));
            $launchDate->delete();
        }
        
    }
}
