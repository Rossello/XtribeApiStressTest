<?php

use xtribe\core\utils\DateTimeHandler;

class XtribeStoreCest
{
    public $store_id;
    public $access_token_store;
    
    public $private_id;
    public $access_token_private;
    
    public function _before(ApiTester $I)
    {
        if(empty($this->access_token_store) ) {
            $this->access_token_store = $I->login('mugshop', 123);
            $this->store_id = $I->grabDataFromResponseByJsonPath('user_id')[0];
        }
        
        $user = $I->make(xtribe\core\models\user\User::class);

        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->private_id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token_private = $I->login($user->username, $user->password);

        $I->authenticate($this->access_token_private);

        for ($i = 0; $i <=5; $i++) {

            $ad = $I->make(xtribe\core\models\ad\Ad::class, [
                'rif_user' => $this->private_id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'date_expiry' => xtribe\core\utils\DateTimeHandler::format(xtribe\core\utils\DateTimeHandler::increaseNowDateTime(6)),
            ]);

            $I->sendPOST("/ads", 
                [
                    'rif_user' => $ad->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad->type])->id,
                    'type' => $ad->type,
                    'name' => $ad->name,
                    'description' => $ad->description,
                    'position_type' => $ad->position_type,
                    'price' => $ad->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad->conditions,
                    'full_price' => $ad->full_price,
                    'date_expiry' => $ad->date_expiry
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );
            $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $I->seeResponseIsJson();
        } 

        $I->amGoingTo("Redeem a promotion for buying xtribe products later");
        $I->sendPUT("/promotions/67/redeem", ['code' => 'VETRINA120']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }

    public function _after(ApiTester $I)
    {
    }
    
    /**
     * Xtribe store product list
     * 
     * GET /xtribestore/products
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function xtribeStoreProductsList(ApiTester $I) {
        
        $I->wantTo("Test the call to retreive the list of products in xtribeStore");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the list of xtribe products in the server");
        $I->sendGET("/xtribestore/products");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of xtribe products in the server with fields param ");
        $I->sendGET("/xtribestore/products",['fields' => 'prod_code,description,credits']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Xtribe purchase slot
     * 
     * POST /xtribestore/orders
     * POST /ads?buy_slot=true
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function xtribeStorePurchaseSlotAd(ApiTester $I) {
        
        $I->wantTo("Test the call for buying a slot for an order");
        $I->authenticate($this->access_token_private);
        
        $ad = $I->make(xtribe\core\models\ad\Ad::class, [
            'rif_user' => $this->private_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            'date_expiry' => xtribe\core\utils\DateTimeHandler::format(xtribe\core\utils\DateTimeHandler::decreaseNowDateTime(6)),
        ]);
        
        $I->amGoingTo("Create a new ad passing the buy_slot param to true on creation");
        $I->sendPOST("/ads?buy_slot=true", 
            [
                'rif_user' => $ad->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad->type])->id,
                'type' => $ad->type,
                'name' => $ad->name,
                'description' => $ad->description,
                'position_type' => $ad->position_type,
                'price' => $ad->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad->conditions,
                'full_price' => $ad->full_price,
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of my ad");
        $I->sendGET("/users/me/ads");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        
        $ad_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->sendPOST("/xtribestore/orders",[
            'rif_user' => $this->private_id,
            'rif_xtribestore_prod' => 'SlotAd',
            'rif_ad' => $ad_id,
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
    }
    
    /**
     * Xtribe purchase navigator
     * 
     * POST /xtribestore/orders
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function xtribeStorePurchaseNavi(ApiTester $I) {
        
        $I->wantTo("Test the call for buying the navigation function for an account");
        $I->authenticate($this->access_token_private);
        
        $I->sendPOST("/xtribestore/orders",[
            'rif_user' => $this->private_id,
            'rif_xtribestore_prod' => 'Navi',
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * Xtribe purchase renew all
     * 
     * POST /xtribestore/orders
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function xtribeStorePurchaseRenewAll(ApiTester $I) {
        
        $I->wantTo("Test the call for buying a renew for all products of the account");
        $I->authenticate($this->access_token_private);
        
        $I->sendPOST("/xtribestore/orders",[
            'rif_user' => $this->private_id,
            'rif_xtribestore_prod' => 'RenewAll',
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * Xtribe purchase renew
     * 
     * POST /xtribestore/orders
     * 
     * @param ApiTester $I
     * @group store-users
     */
    public function xtribeStorePurchaseRenew(ApiTester $I) {
        
        $I->wantTo("Test the call for buying a renew for a product of the account");
        $I->authenticate($this->access_token_store);
        
        $ad = $I->make(xtribe\core\models\ad\Ad::class, [
            'rif_user' => $this->store_id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            'date_expiry' => xtribe\core\utils\DateTimeHandler::format(xtribe\core\utils\DateTimeHandler::decreaseNowDateTime(4)),
        ]);

        $I->sendPOST("/ads", 
            [
                'rif_user' => $ad->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad->type])->id,
                'type' => $ad->type,
                'name' => $ad->name,
                'description' => $ad->description,
                'position_type' => $ad->position_type,
                'price' => $ad->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad->conditions,
                'full_price' => $ad->full_price,
                'date_expiry' => $ad->date_expiry
            ], 
            ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $ad_id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->amGoingTo("Purchase an ad Renew");
        $I->sendPOST("/xtribestore/orders",[
            'rif_user' => $this->store_id,
            'rif_xtribestore_prod' => 'Renew',
            'rif_ad' => $ad_id,
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * Xtribe purchase showcase
     * 
     * POST /xtribestore/orders
     * 
     * @param ApiTester $I
     * @group store-users
     */
    public function xtribeStorePurchaseShowcase(ApiTester $I) {
        
        $I->wantTo("Test the call for buying a showcase for all prodcuts of the account");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the list of my ad");
        $I->sendGET("/users/me/ads");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        
        $ad_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->sendPOST("/xtribestore/orders",[
            'rif_user' => $this->private_id,
            'rif_xtribestore_prod' => 'Showcase',
            'rif_ad' => $ad_id,
            'rif_geo_area' => 101 // Milano 
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
}
