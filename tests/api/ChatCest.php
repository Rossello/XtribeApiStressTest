<?php 

class ChatCest
{
    public $private_id;
    public $access_token_private;
    
    
    public function _before(ApiTester $I)
    {
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->private_id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token_private = $I->login($user->username, $user->password);
    }

    /**
     * Upload an image for the chat
     * 
     * GET /showcase/geoareas
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function uploadImage(ApiTester $I)
    {
        $I->wantTo("Test the call to upload an image");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Upload an image");
        $I->sendGET("/showcase/geoareas");
        $I->sendPOST("/chat/images",[],['image_file' => codecept_data_dir('logo-240x69.png')]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
}
