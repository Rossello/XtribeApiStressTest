<?php

use League\FactoryMuffin\Faker\Facade as Faker;

class UserCest {

    public $user;
    
    public $deal_id;
    public $deal_ad_id;
    public $deal_user;


    public $access_token;
    
    public function _before(ApiTester $I) {
        $user = $I->make(xtribe\core\models\user\User::class);

        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->user = $user;
        $this->user->id = $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token = $I->login($this->user->username, $this->user->password);
        
    }

    public function _after(ApiTester $I) {
        
    }
    
    /**
     * User creation
     * 
     * POST /users
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userRegistration(ApiTester $I) {
        
        $I->wantTo("Test the user registration ");
        
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * User profile
     * 
     * GET /users/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userProfile(ApiTester $I) {

        $I->wantTo("Test if I can receive the detail of an user and the detail of myself");
        $I->authenticate($this->access_token);

        $I->amGoingTo("Receive information about my user");
        $I->sendGET("/users/me");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        $I->amGoingTo("Receive information about the user with id 1");
        $I->sendGET("/users/1");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->sendGET('/users/me', ['expand' => 'store']);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->amGoingTo("see if there is a json with store informations");
        $I->seeResponseJsonMatchesJsonPath('store');
    }
    
    /**
     * User profile update
     * 
     * PUT /users/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userUpdateProfile(ApiTester $I) {

        $I->wantTo("Test if I can update my account and can't update other user informations");
        $I->authenticate($this->access_token);

        $I->amGoingTo("Update my account's informations");
        $I->sendPUT('/users/me', ['name' => 'Test']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        $I->amGoingTo("Update other account's informations");
        $I->sendPUT('/users/2', ['name' => $this->user->name]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
    }
    
    /**
     * User add a followed user
     * 
     * PUT /users/{$ID}/block
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userBlockProfile(ApiTester $I) {
        
        $I->wantTo("Test if I can block my own account");
        $I->authenticate($this->access_token);

        $I->amGoingTo("Block another user");
        $I->sendPUT("/users/1/block");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();

        $I->amGoingTo("Block myself");
        $I->sendPUT("/users/me/block");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":true}');
    }
    
    /**
     * User register device
     * 
     * POST /users/{$ID}/devices
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userRegisterDevice(ApiTester $I) {
        
        $I->wantTo("Test the registration of an user's device");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Register one of my devices");
        $user_device = $I->make(xtribe\core\models\user\UserDevice::class,[
            'rif_user'  => $this->user->id,
            'device_os_version' => '11.1.2',
            'device_model' => 'iPhoneX',
            'client_version' => '2.1.10',
        ]);
        
        $I->sendPOST("/users/{$this->user->id}/devices",[
            'rif_user'  => $user_device->rif_user,
            'device_os' => $user_device->device_os,
            'device_os_version' => $user_device->device_os_version,
            'device_uid' => $user_device->device_uid,
            'device_token' => $user_device->device_token,
            'client_version' => $user_device->client_version,
            'device_model' => $user_device->device_model,
            'environment' => $user_device->environment
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * User credit balance
     * 
     * GET /users/{$ID}/balance
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userCreditBalance(ApiTester $I) {

        $I->wantTo("Test if I can only see my own credit balance");
        $I->authenticate($this->access_token);

        $I->amGoingTo("See my account balance");
        $I->sendGET("/users/me/balance");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        $I->amGoingTo("See other user's account balance");
        $I->sendGET("/users/1/balance");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
    }
    
    /**
     * User transactions
     * 
     * GET /users/{$ID}/transactions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userTransactions(ApiTester $I) {

        $I->wantTo("Test if I can see my own transactions");
        $I->authenticate($this->access_token);

        $I->amGoingTo("See my transactions");
        $I->sendGET("/users/me/transactions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();

        $I->amGoingTo("See other user's transactions");
        $I->sendGET("/users/1/transactions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
    }
    
    /**
     * User personal ads
     * 
     * GET /users/{$ID}/ads
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userPersonalAds(ApiTester $I) {

        $I->wantTo("Test if I can receive users personal ads");
        $I->authenticate($this->access_token);

        $I->amGoingTo("See my Ads");
        $I->sendGET("/users/me/ads");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Expand Category");
        $I->sendGET('/users/me/ads', ['expand' => 'category']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User favorite ads
     * 
     * GET /users/{$ID}/ads/favorites
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userFavoriteAds(ApiTester $I) {

        $I->wantTo("Test if I can receive users favorite ads");
        $I->authenticate($this->access_token);

        $I->amGoingTo("See my account's favorites ads");
        $I->sendGET("/users/me/ads/favorites");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/ads/favorites", ['expand' => 'user']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User edit favorites
     * 
     * POST     /users/{$ID}/ads/favorites
     * DELETE   /users/{$ID}/ads/favorites/{$FID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userEditFavorites(ApiTester $I) {

        $I->wantTo("Test if I can add and delete an Ad from Favorites");
        $I->authenticate($this->access_token);
        
        // Create a dummy ad,
        // All the tests that are involving ads are in the AdCest file
        $ad = $I->make(xtribe\core\models\ad\Ad::class, [
            'rif_user' => $this->user->id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);

        $I->sendPOST("/ads", [
            'rif_user' => $ad->rif_user,
            'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad->type])->id,
            'type' => $ad->type,
            'name' => $ad->name,
            'description' => $ad->description,
            'position_type' => $ad->position_type,
            'price' => $ad->price,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            'conditions' => $ad->conditions,
            'full_price' => $ad->full_price
                ], ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);

        // Grab the id
        $id = $I->grabDataFromResponseByJsonPath('id')[0];

        $I->wantToTest('Add an Ad to favorites');
        $I->sendPOST('/users/me/ads/favorites', [
            'rif_user' => $this->user->id,
            'rif_ad' => $id
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $favorite_id = $I->grabDataFromResponseByJsonPath('id')[0];
        $I->wantToTest('Remove an Ad to favorites');
        $I->sendDELETE("/users/me/ads/favorites/{$favorite_id}");

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
    }
    
    /**
     * User deals
     * 
     * GET  /users/{$ID}/deals
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userDeals(ApiTester $I) {

        $I->wantTo("Test if I can receive users deals");
        $I->authenticate($this->access_token);

        $I->sendGET("/users/me/deals");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User open deals
     * 
     * GET  /users/{$ID}/deals/open
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userOpenDeals(ApiTester $I) {

        $I->wantTo("Test if I can receive users open deals");
        $I->authenticate($this->access_token);

        $I->sendGET("/users/me/deals/open");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User closed deals
     * 
     * GET  /users/{$ID}/deals/closed
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userClosedDeals(ApiTester $I) {

        $I->wantTo("Test if I can receive users closed deals");
        $I->authenticate($this->access_token);

        $I->sendGET("/users/me/deals/closed");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User add a purchase proposal deal action.
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionPurchaseProposal(ApiTester $I) {
        
        $I->wantTo("Test if I can create a deal action");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $user = $S->make(xtribe\core\models\user\User::class);
            $S->sendPOST("/users", [
                'e_mail' => $user->e_mail,
                'username' => $user->username,
                'password' => $user->password,
                'position_type' => $user->position_type,
                'lat' => $user->lat,
                'lng' => $user->lng,
                'check_in' => $user->check_in,
                'date_of_birth' => $user->date_of_birth,
                'place_of_birth' => $user->place_of_birth,
                'state' => $user->state,
                'city' => $user->city,
                'zip_code' => $user->zip_code,
                'address' => $user->address,
                'name' => $user->name,
                'surname' => $user->surname,
                'gender' => 'M',
                'profile_status' => $user->profile_status,
                'phone' => $user->phone,
                'default_currency' => $user->default_currency,
                'default_language' => $user->default_language,
                'default_timezone' => $user->default_timezone,
                'default_country' => $user->default_country
            ]);
            
            
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_user = $user;
            
            $this->deal_user->id = $S->grabDataFromResponseByJsonPath('id')[0];
            
            $access_token = $S->login($user->username, $user->password);
            $S->authenticate($access_token);
            
            $ad_private = $S->make(xtribe\core\models\ad\Ad::class,[
                'rif_user' => $this->deal_user->id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            ]);
            
            $S->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                    'type' => $ad_private->type,
                    'name' => $ad_private->name,
                    'description' => $ad_private->description,
                    'position_type' => $ad_private->position_type,
                    'price' => $ad_private->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private->conditions,
                    'full_price' => $ad_private->full_price
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_ad_id = $S->grabDataFromResponseByJsonPath('id')[0];
            
        });
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PURCHASE_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->deal_user->id,
            'rif_ad' => $this->deal_ad_id,
            'price' => 100,
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals",['expand' => 'ad']);
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $access_token = $S->login($this->deal_user->username, $this->deal_user->password);
            $S->authenticate($access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_REJECTED,
                'rif_sender_user' => $this->deal_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
    }
    
    /**
     * User add a rental proposal deal action.
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionRentalProposal(ApiTester $I) {
        
        $I->wantTo("Test if I can create a rental proposal deal action");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $user = $S->make(xtribe\core\models\user\User::class);
            $S->sendPOST("/users", [
                'e_mail' => $user->e_mail,
                'username' => $user->username,
                'password' => $user->password,
                'position_type' => $user->position_type,
                'lat' => $user->lat,
                'lng' => $user->lng,
                'check_in' => $user->check_in,
                'date_of_birth' => $user->date_of_birth,
                'place_of_birth' => $user->place_of_birth,
                'state' => $user->state,
                'city' => $user->city,
                'zip_code' => $user->zip_code,
                'address' => $user->address,
                'name' => $user->name,
                'surname' => $user->surname,
                'gender' => 'M',
                'profile_status' => $user->profile_status,
                'phone' => $user->phone,
                'default_currency' => $user->default_currency,
                'default_language' => $user->default_language,
                'default_timezone' => $user->default_timezone,
                'default_country' => $user->default_country
            ]);
            
            
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_user = $user;
            
            $this->deal_user->id = $S->grabDataFromResponseByJsonPath('id')[0];
            
            $access_token = $S->login($user->username, $user->password);
            $S->authenticate($access_token);
            
            $ad_private = $S->make(xtribe\core\models\ad\Ad::class,[
                'rif_user' => $this->deal_user->id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            ]);
            
            $S->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                    'type' => $ad_private->type,
                    'name' => $ad_private->name,
                    'description' => $ad_private->description,
                    'position_type' => $ad_private->position_type,
                    'price' => $ad_private->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private->conditions,
                    'full_price' => $ad_private->full_price
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_ad_id = $S->grabDataFromResponseByJsonPath('id')[0];
            
        });
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_RENTAL_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->deal_user->id,
            'price' => 100,
            'start_date' => \xtribe\core\utils\DateTimeHandler::format(\xtribe\core\utils\DateTimeHandler::now()),
            'end_date' => \xtribe\core\utils\DateTimeHandler::format(\xtribe\core\utils\DateTimeHandler::increaseNowDateTime(7)),
            'rif_ad' => $this->deal_ad_id,
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals");
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $access_token = $S->login($this->deal_user->username, $this->deal_user->password);
            $S->authenticate($access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_REJECTED,
                'rif_sender_user' => $this->deal_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
        
    }
    
    /**
     * User add a barter proposal deal action.
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionBarterProposal(ApiTester $I) {
        
        $I->wantTo("Test if I can create a barter proposal deal action");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $ad = $I->make(xtribe\core\models\ad\Ad::class, [
            'rif_user' => $this->user->id,
            'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
            'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
        ]);

        $I->sendPOST("/ads", [
                'rif_user' => $ad->rif_user,
                'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad->type])->id,
                'type' => $ad->type,
                'name' => $ad->name,
                'description' => $ad->description,
                'position_type' => $ad->position_type,
                'price' => $ad->price,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                'conditions' => $ad->conditions,
                'full_price' => $ad->full_price
            ], ['image_file' => codecept_data_dir('logo-240x69.png')]
        );
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        
        $barter_ad_id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $user = $S->make(xtribe\core\models\user\User::class);
            $S->sendPOST("/users", [
                'e_mail' => $user->e_mail,
                'username' => $user->username,
                'password' => $user->password,
                'position_type' => $user->position_type,
                'lat' => $user->lat,
                'lng' => $user->lng,
                'check_in' => $user->check_in,
                'date_of_birth' => $user->date_of_birth,
                'place_of_birth' => $user->place_of_birth,
                'state' => $user->state,
                'city' => $user->city,
                'zip_code' => $user->zip_code,
                'address' => $user->address,
                'name' => $user->name,
                'surname' => $user->surname,
                'gender' => 'M',
                'profile_status' => $user->profile_status,
                'phone' => $user->phone,
                'default_currency' => $user->default_currency,
                'default_language' => $user->default_language,
                'default_timezone' => $user->default_timezone,
                'default_country' => $user->default_country
            ]);
            
            
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_user = $user;
            
            $this->deal_user->id = $S->grabDataFromResponseByJsonPath('id')[0];
            
            $access_token = $S->login($user->username, $user->password);
            $S->authenticate($access_token);
            
            $ad_private = $S->make(xtribe\core\models\ad\Ad::class,[
                'rif_user' => $this->deal_user->id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            ]);
            
            $S->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                    'type' => $ad_private->type,
                    'name' => $ad_private->name,
                    'description' => $ad_private->description,
                    'position_type' => $ad_private->position_type,
                    'price' => $ad_private->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private->conditions,
                    'full_price' => $ad_private->full_price
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_ad_id = $S->grabDataFromResponseByJsonPath('id')[0];
        });
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_BARTER_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->deal_user->id,
            'rif_ad' => $this->deal_ad_id,
            'rif_other_ad' => $barter_ad_id
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals");
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $access_token = $S->login($this->deal_user->username, $this->deal_user->password);
            $S->authenticate($access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_REJECTED,
                'rif_sender_user' => $this->deal_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
    }
    
    /**
     * User add a feedback action for a deal
     * 
     * POST /users/{$ID}/deals/actions
     * GET  /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddDealActionFeedback(ApiTester $I) {
        
        $I->wantTo("Test if I can create a deal action");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $user = $S->make(xtribe\core\models\user\User::class);
            $S->sendPOST("/users", [
                'e_mail' => $user->e_mail,
                'username' => $user->username,
                'password' => $user->password,
                'position_type' => $user->position_type,
                'lat' => $user->lat,
                'lng' => $user->lng,
                'check_in' => $user->check_in,
                'date_of_birth' => $user->date_of_birth,
                'place_of_birth' => $user->place_of_birth,
                'state' => $user->state,
                'city' => $user->city,
                'zip_code' => $user->zip_code,
                'address' => $user->address,
                'name' => $user->name,
                'surname' => $user->surname,
                'gender' => 'M',
                'profile_status' => $user->profile_status,
                'phone' => $user->phone,
                'default_currency' => $user->default_currency,
                'default_language' => $user->default_language,
                'default_timezone' => $user->default_timezone,
                'default_country' => $user->default_country
            ]);
            
            
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_user = $user;
            
            $this->deal_user->id = $S->grabDataFromResponseByJsonPath('id')[0];
            
            $access_token = $S->login($user->username, $user->password);
            $S->authenticate($access_token);
            
            $ad_private = $S->make(xtribe\core\models\ad\Ad::class,[
                'rif_user' => $this->deal_user->id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            ]);
            
            $S->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                    'type' => $ad_private->type,
                    'name' => $ad_private->name,
                    'description' => $ad_private->description,
                    'position_type' => $ad_private->position_type,
                    'price' => $ad_private->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private->conditions,
                    'full_price' => $ad_private->full_price
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_ad_id = $S->grabDataFromResponseByJsonPath('id')[0];
            
        });
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PURCHASE_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->deal_user->id,
            'rif_ad' => $this->deal_ad_id,
            'price' => 100,
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals",['expand' => 'ad']);
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $access_token = $S->login($this->deal_user->username, $this->deal_user->password);
            $S->authenticate($access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_ACCEPTED,
                'rif_sender_user' => $this->deal_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_BUYER_FEEDBACK,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->deal_user->id,
            'feedback_value' => 1,
            'feedback_message' => 'Good Feedback Dude', 
            'rif_deal' => $this->deal_id
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $access_token = $S->login($this->deal_user->username, $this->deal_user->password);
            $S->authenticate($access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_SELLER_FEEDBACK,
                'rif_sender_user' => $this->deal_user->id,
                'rif_receiver_user' => $this->user->id,
                'feedback_value' => 1,
                'feedback_message' => 'Ehy Thanks, good feedback too', 
                'rif_deal' => $this->deal_id
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
        });
    }
    
    /**
     * User read deal actions
     * 
     * PUT /users/{$ID}/deals/{$DID}/actions/read
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userSetDealActionsAsRead(ApiTester $I) {
     
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $user = $S->make(xtribe\core\models\user\User::class);
            $S->sendPOST("/users", [
                'e_mail' => $user->e_mail,
                'username' => $user->username,
                'password' => $user->password,
                'position_type' => $user->position_type,
                'lat' => $user->lat,
                'lng' => $user->lng,
                'check_in' => $user->check_in,
                'date_of_birth' => $user->date_of_birth,
                'place_of_birth' => $user->place_of_birth,
                'state' => $user->state,
                'city' => $user->city,
                'zip_code' => $user->zip_code,
                'address' => $user->address,
                'name' => $user->name,
                'surname' => $user->surname,
                'gender' => 'M',
                'profile_status' => $user->profile_status,
                'phone' => $user->phone,
                'default_currency' => $user->default_currency,
                'default_language' => $user->default_language,
                'default_timezone' => $user->default_timezone,
                'default_country' => $user->default_country
            ]);
            
            
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_user = $user;
            
            $this->deal_user->id = $S->grabDataFromResponseByJsonPath('id')[0];
            
            $access_token = $S->login($user->username, $user->password);
            $S->authenticate($access_token);
            
            $ad_private = $S->make(xtribe\core\models\ad\Ad::class,[
                'rif_user' => $this->deal_user->id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            ]);
            
            $S->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                    'type' => $ad_private->type,
                    'name' => $ad_private->name,
                    'description' => $ad_private->description,
                    'position_type' => $ad_private->position_type,
                    'price' => $ad_private->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private->conditions,
                    'full_price' => $ad_private->full_price
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_ad_id = $S->grabDataFromResponseByJsonPath('id')[0];
            
        });
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PURCHASE_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->deal_user->id,
            'rif_ad' => $this->deal_ad_id,
            'price' => 100,
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals",['expand' => 'ad']);
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $otherUser->does(function(ApiTester $S) {
            
            $S->amGoingTo("Authenticate my user");
            
            $access_token = $S->login($this->deal_user->username, $this->deal_user->password);
            $S->authenticate($access_token);
            
            $params = [
                'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PROPOSAL_ACCEPTED,
                'rif_sender_user' => $this->deal_user->id,
                'rif_receiver_user' => $this->user->id,
                'rif_deal' => $this->deal_id,
            ];

            $S->sendPOST("/users/me/deals/actions", $params);
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
        });
        
        $I->sendGET("/users/me/deals/$this->deal_id/actions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $action_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        $I->sendPUT("/users/me/deals/$this->deal_id/actions/read");
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":true}');
    }
    
    /**
     * User filter deal actions
     * 
     * GET /users/{$ID}/deals/{$DID}/actions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userFilterDealActions(ApiTester $I) {
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);

        $otherUser = $I->haveFriend('Other User');
        $otherUser->does(function(ApiTester $S) {
            
            $user = $S->make(xtribe\core\models\user\User::class);
            $S->sendPOST("/users", [
                'e_mail' => $user->e_mail,
                'username' => $user->username,
                'password' => $user->password,
                'position_type' => $user->position_type,
                'lat' => $user->lat,
                'lng' => $user->lng,
                'check_in' => $user->check_in,
                'date_of_birth' => $user->date_of_birth,
                'place_of_birth' => $user->place_of_birth,
                'state' => $user->state,
                'city' => $user->city,
                'zip_code' => $user->zip_code,
                'address' => $user->address,
                'name' => $user->name,
                'surname' => $user->surname,
                'gender' => 'M',
                'profile_status' => $user->profile_status,
                'phone' => $user->phone,
                'default_currency' => $user->default_currency,
                'default_language' => $user->default_language,
                'default_timezone' => $user->default_timezone,
                'default_country' => $user->default_country
            ]);
            
            
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_user = $user;
            
            $this->deal_user->id = $S->grabDataFromResponseByJsonPath('id')[0];
            
            $access_token = $S->login($user->username, $user->password);
            $S->authenticate($access_token);
            
            $ad_private = $S->make(xtribe\core\models\ad\Ad::class,[
                'rif_user' => $this->deal_user->id,
                'position_type' => xtribe\core\models\ad\enum\AdPositionType::AD_POSITION_TYPE_MOBILE,
                'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
            ]);
            
            $S->sendPOST("/ads", 
                [
                    'rif_user' => $ad_private->rif_user,
                    'rif_category' => \xtribe\core\models\ad\AdCategory::findOne(['type' => $ad_private->type])->id,
                    'type' => $ad_private->type,
                    'name' => $ad_private->name,
                    'description' => $ad_private->description,
                    'position_type' => $ad_private->position_type,
                    'price' => $ad_private->price,
                    'currency' => xtribe\core\models\enum\Currency::CURRENCY_EURO,
                    'conditions' => $ad_private->conditions,
                    'full_price' => $ad_private->full_price
                ], 
                ['image_file' => codecept_data_dir('logo-240x69.png')]
            );
            $S->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $S->seeResponseIsJson();
            
            $this->deal_ad_id = $S->grabDataFromResponseByJsonPath('id')[0];
            
        });
        
        $params = [
            'type' => \xtribe\core\models\deal\enum\DealActionType::DEAL_ACTION_TYPE_PURCHASE_PROPOSAL,
            'rif_sender_user' => $this->user->id,
            'rif_receiver_user' => $this->deal_user->id,
            'rif_ad' => $this->deal_ad_id,
            'price' => 100,
        ];
        
        $I->sendPOST("/users/me/deals/actions", $params);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/deals",['expand' => 'ad']);
        $this->deal_id = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->sendGET("/users/me/deals/$this->deal_id/actions", [
            'filters' => [
                [
                    'field' => 'is_read',
                    'value' => 0,
                    'condition' => 'eq'
                ]
            ]
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User get followers 
     * 
     * GET /users/{$ID}/followers
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userGetFollowersUsers(ApiTester $I) {
        
        $I->wantTo("Test if I can get followers");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $I->sendGET("/users/me/followers");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User get followed
     * 
     * GET /users/{$ID}/followed
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userGetFollowedUsers(ApiTester $I) {
        
        $I->wantTo("Test if I can get followed users");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $I->sendGET("/users/me/followed");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * User add a followed user
     * 
     * POST /users/{$ID}/followed
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userAddFollowedUser(ApiTester $I) {
    
        $I->wantTo("Test if I can start to follow an user");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->sendPOST("/users/me/followed",[
            'rif_user_follower' => $this->user->id,
            'rif_user_followed' => $id
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
    }
    
    /**
     * User add a followed user
     * 
     * DELETE /users/{$ID}/followed
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userRemoveFollowedUser(ApiTester $I) {
    
        $I->wantTo("Test if I can stop to follow an user");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        $I->sendPOST("/users/me/followed",[
            'rif_user_follower' => $this->user->id,
            'rif_user_followed' => $id
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $I->sendGET("/users/me/followed");
        $id_followed = $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
         
        $I->sendDELETE("/users/me/followed/$id_followed");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::NO_CONTENT);
    }
    
    /**
     * User add a followed user
     * 
     * GET /users/suggested
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function userSuggestedUsers(ApiTester $I) {
        
        $I->wantTo("Test if I can get suggested users");
        
        $I->amGoingTo("Authenticate my user");
        $I->authenticate($this->access_token);
        
        $I->sendGET("/users/suggested");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
}
