<?php


class TagCest {
    
    public $user;
    
    public $access_token;
    
    public function _before(ApiTester $I)
    {
        $user = $I->make(xtribe\core\models\user\User::class);

        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->user = $user;
        $this->access_token = $I->login($user->username, $user->password);
    }
    
    /**
     * Tag suggest
     * 
     * GET /tags/suggest
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function tagSuggest(ApiTester $I) {
        
        $I->wantTo("Test the calls to the node that suggest tags");
        $I->authenticate($this->access_token);
         
        $I->sendGET('/tags/suggest',[
            'text' => 'mot'
        ]);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
}
