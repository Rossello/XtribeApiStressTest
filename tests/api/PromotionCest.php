<?php


class PromotionCest
{
    public $private_id;
    public $access_token_private;
    
    public function _before(ApiTester $I)
    {
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->private_id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token_private = $I->login($user->username, $user->password);
    }
    
    public function _after(ApiTester $I)
    {
        
    }
    
    /**
     * Promotion list
     * 
     * GET /promotions
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function promotionList(ApiTester $I)  {
        
        $I->wantTo("Test the call to promotion list node");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the list of promotions in the server");
        $I->sendGET("/promotions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of promotions in the server with field params");
        $I->sendGET("/promotions",['field' => 'id,date_insert,status']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the list of promotions in the server with field params");
        $I->sendGET("/promotions",['country_code' => 'IT']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Promotion detail
     * 
     * GET /promotions/{$ID}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function promotionDetail(ApiTester $I) {
        
        $I->wantTo("Test the call to promotion detail node");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Receive the list of promotions in the server");
        $I->sendGET("/promotions");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $promotion_id= $I->grabDataFromResponseByJsonPath('$.items[0].id')[0];
        
        $I->amGoingTo("Receive the id detail of the promotion in the server");
        $I->sendGET("/promotions/$promotion_id");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of the promotion in the server with field params");
        $I->sendGET("/promotions/$promotion_id", ['field' => 'name,description,price']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Promotion redeem
     * 
     * GET /promotions/{$ID}/redeem
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function promotionRedeem(ApiTester $I) {
        
        $I->wantTo("Test the call for redeem ");
        $I->authenticate($this->access_token_private);
        
        $I->amGoingTo("Redeem the promotion with id 67");
        $I->sendPUT("/promotions/67/redeem", ['code' => 'VETRINA120']);
        
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
}
