<?php 

class GamificationCest
{
    public $id;
    public $access_token;
    
    /**
     * Test the call for the ad list
     * @param ApiTester $I
     */
    public function _before(ApiTester $I) {
        
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->private_id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token = $I->login($user->username, $user->password);
    }

    /**
     * Gamification target list
     * 
     * GET /gamification/targets
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function targetList(ApiTester $I) {
        
        $I->wantTo("Test the call to the target node");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Get all the targets");
        $I->sendGET("/gamification/targets");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Get all the targets specifiyng the expand parameter");
        $I->sendGET("/gamification/targets", ['expand' => 'goals']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Gamification target list
     * 
     * GET /gamification/targets/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function targetDetail(ApiTester $I) {
        
        $I->wantTo("Test the call to the target detail node");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Get all the targets");
        $I->sendGET("/gamification/targets");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->amGoingTo("Get the detail of a target");
        $I->sendGET("/gamification/targets/$code");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $I->amGoingTo("Receive the detail of a target in the server specifiyng the expand parameter");
        $I->sendGET("/gamification/targets/$code", ['expand' => 'goals']);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    /**
     * Gamification goal list
     * 
     * GET /gamification/goals
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function goalList(ApiTester $I) {
        
        $I->wantTo("Test the call to the goal node");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Get all the goals");
        $I->sendGET("/gamification/goals");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
    }
    
    /**
     * Gamification goal detail
     * 
     * GET /gamification/goals/{$CODE}
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function goalDetail(ApiTester $I) {
        
        $I->wantTo("Test the call to the goal detail node");
        $I->authenticate($this->access_token);
        
        $I->amGoingTo("Get all the goals");
        $I->sendGET("/gamification/goals");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $code = $I->grabDataFromResponseByJsonPath('$.items[0].code')[0];
        
        $I->amGoingTo("Get the detail of a goal");
        $I->sendGET("/gamification/goals/$code");
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    
    
}
