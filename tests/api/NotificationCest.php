<?php 

class NotificationCest
{
    
    public $id;
    public $access_token;
    
    
    public function _before(ApiTester $I)
    {
        $user = $I->make(xtribe\core\models\user\User::class);
        $I->amGoingTo("Save user throught api");
        $I->sendPOST("/users", [
            'e_mail' => $user->e_mail,
            'username' => $user->username,
            'password' => $user->password,
            'position_type' => $user->position_type,
            'lat' => $user->lat,
            'lng' => $user->lng,
            'check_in' => $user->check_in,
            'date_of_birth' => $user->date_of_birth,
            'place_of_birth' => $user->place_of_birth,
            'state' => $user->state,
            'city' => $user->city,
            'zip_code' => $user->zip_code,
            'address' => $user->address,
            'name' => $user->name,
            'surname' => $user->surname,
            'gender' => 'M',
            'profile_status' => $user->profile_status,
            'phone' => $user->phone,
            'default_currency' => $user->default_currency,
            'default_language' => $user->default_language,
            'default_timezone' => $user->default_timezone,
            'default_country' => $user->default_country
        ]);

        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();

        $this->id= $I->grabDataFromResponseByJsonPath('id')[0];
        $this->access_token = $I->login($user->username, $user->password);
        
    }
    
    /**
     * Gamification target list
     * 
     * GET /users/{$ID}/notifications
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function test120Notification(ApiTester $I)
    {
        $I->am("User1");
        $I->wantTo("Login in the application");
        
        $I->authenticate($this->access_token);
        
        $user_model = $I->make(xtribe\core\models\user\User::class);
        $I->sendPOST("/users", [
               'e_mail' => $user_model->e_mail,
               'username' => $user_model->username,
               'password' => $user_model->password,
               'position_type' => $user_model->position_type,
               'lat' => $user_model->lat,
               'lng' => $user_model->lng,
               'check_in' => $user_model->check_in,
               'date_of_birth' => $user_model->date_of_birth,
               'place_of_birth' => $user_model->place_of_birth,
               'state' => $user_model->state,
               'city' => $user_model->city,
               'zip_code' => $user_model->zip_code,
               'address' => $user_model->address,
               'name' => $user_model->name,
               'surname' => $user_model->surname,
               'gender' => 'M',
               'profile_status' => $user_model->profile_status,
               'phone' => $user_model->phone,
               'default_currency' => $user_model->default_currency,
               'default_language' => $user_model->default_language,
               'default_timezone' => $user_model->default_timezone,
               'default_country' => $user_model->default_country
           ]);
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        
        $id = $I->grabDataFromResponseByJsonPath('id')[0];
        
        
        $user2 = $I->haveFriend('User2');
        
        $user2->does(function(ApiTester $I) use ($user_model, $id){
            
            $login_data = $I->login($user_model->username, $user_model->password);
            $access_token = $I->grabDataFromResponseByJsonPath('access_token')[0];
            
            $I->authenticate($access_token);
            
            $I->amGoingTo("Start following User1"); // Every move you make. 
            $I->sendPOST("/users/me/followed",[
                'rif_user_follower' => $id,
                'rif_user_followed' => $this->id
            ]);
            
            $I->seeResponseCodeIs(Codeception\Util\HttpCode::CREATED);
            $I->seeResponseIsJson();
            
        });
        
        $I->sendGET('/users/me/notifications');
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        
        $type = $I->grabDataFromResponseByJsonPath('$.items[0].type')[0];
        $I->seeResponseContainsJson(['items' => ['type' => xtribe\core\models\notification\enum\NotificationType::NEW_FOLLOWER]]);
    }
    
    /**
     * Gamification target list
     * 
     * GET /users/{$ID}/notifications/read
     * 
     * @param ApiTester $I
     * @group private-users
     */
    public function readAllNotifications(ApiTester $I) 
    {
        $I->wantTo("Check if I can read all the notifications");
        $I->authenticate($this->access_token);
        
        $I->sendPUT('/users/me/notifications/read');
        $I->seeResponseCodeIs(Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['result' => TRUE]);
        
    }
}
