<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks 
{
    
    
    public function parallelRun($numberOfParallelLaunches = 5)
    {
        $parallel = $this->taskParallelExec();
        for ($i = 1; $i <= $numberOfParallelLaunches; $i++) {
            $parallel->process(
                $this->taskCodecept()
                    ->xml("tests/_log/result_$i.xml")
                    ->suite('api')
                    ->test('StressCest')
            );
        }
        return $parallel->run();
    }
    
    function parallelMergeResults()
    {
        $merge = $this->taskMergeXmlReports();
        for ($i=1; $i<=5; $i++) {
            $merge->from("tests/_output/result_paracept_$i.xml");
        }
        $merge->into("tests/_output/result_paracept.xml")->run();
    }
    
}