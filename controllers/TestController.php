<?php

namespace app\controllers;

use yii\web\Controller;

use xtribe\core\utils\DateTimeHandler;

use xtribe\core\models\deal\Deal;
use xtribe\core\models\deal\DealAction;

use xtribe\core\models\ad\Favorite;
use xtribe\core\models\ad\AdReport;
use xtribe\core\models\ad\AdImage;
use xtribe\core\models\ad\Ad;
use xtribe\core\models\ad\AdTag;

use xtribe\core\models\ad\elasticsearch\Ad as AdElastic;

use xtribe\core\models\xtribestore\XtribestoreOrder;

use xtribe\core\models\user\User;
use xtribe\core\models\user\UserDevice;
use xtribe\core\models\user\UserFollower;
use xtribe\core\models\gamification\UserTarget;
use xtribe\core\models\gamification\UserGoal;

use xtribe\core\models\promotion\PromotionCodeUsage;

use xtribe\core\models\tag\Tag;


class TestController extends Controller 
{
    
    public function actionClean($date) {
        
        try {
            //1. Formatting passed date.
            $formattedDate = DateTimeHandler::format(DateTimeHandler::dateTime($date));

            //2. Starting the deletion
            /**
             * 
             *  DELETING DEALS 
             *   
             *    Order : 
             *    
             *    - Deal Action.
             *    - Deal.
             */

            //Deal actions 
            DealAction::deleteAll(['>=', 'deal_action.date_insert', $formattedDate]);

            //Deals
            Deal::deleteAll(['>=', 'deal.date_insert', $formattedDate]);

            /**
             * 
             * XTRIBESTORE ORDERS
             *  
             *  Oder:
             * 
             *  - Xtribestore orders
             * 
             */


            // Xtribestore orders
            XtribestoreOrder::deleteAll(['>=', 'xtribestore_order.date_insert', $formattedDate]);

            /**
             * 
             *  PROMOTIONS CODES USAGES
             *      
             *    Order :     
             *       
             *    - Promotin code usage.
             */

            // Promotion code usage
            PromotionCodeUsage::deleteAll(['>=','promotion_code_usage.date_insert',$formattedDate]);

            
            /**
             * 
             *  ADS
             *      
             *    Order :     
             *       
             *    - Ad images.
             *    - Ad reports.
             *    - Favorites
             *    - Ad tags
             *    - Ads
             */

            //Ad images.
            AdImage::deleteAll(['>=', 'ad_image.date_insert', $formattedDate]);

            //Ad reports
            AdReport::deleteAll(['>=', 'ad_report.date_insert', $formattedDate]);

            //Ad tag
            AdTag::deleteAll(['>=', 'ad_tag.date_insert', $formattedDate]);

            //Favorites
            Favorite::deleteAll(['>=', 'favorite.date_insert', $formattedDate]);

            //Ad
            Ad::deleteAll(['>=', 'ad.date_insert', $formattedDate]);
            
            //AdElastic::deleteAll(['>=', 'ad.date_insert', $formattedDate]);
            
            
            /**
             * 
             *  TAGS
             *      
             *    Order :     
             *       
             *    - Tags.
             */

            // Tag 
            Tag::deleteAll(['>=', 'tag.date_insert', $formattedDate]); 

            

            /**
             * 
             *  USER
             *      
             *    Order :     
             *       
             *      - User devices.
             *      - User followers.
             *      - User targets.
             *      - User goals.
             *      - User.
             * 
             */

            // User device
            UserDevice::deleteAll(['>=', 'user_device.date_insert', $formattedDate]);

            // User follower
            UserFollower::deleteAll(['>=', 'user_follower.date_insert', $formattedDate]);

            // User target
            UserTarget::deleteAll(['>=', 'user_target.date_insert', $formattedDate]);

            // User goal
            UserGoal::deleteAll(['>=', 'user_goal.date_insert', $formattedDate]);


            /**
             * Internal token table doesn't have a date_insert field.
             * 
             * Deletting mannually.
             * 
             */
            User::deleteAll(['>=', 'user.register_date', $formattedDate]);

            return "COMPLETED! - $formattedDate";
            
        } catch (\Exception $exc) {
            
            return $exc->getMessage();
            
        }
        
    }
}
