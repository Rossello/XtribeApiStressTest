<?php


namespace app\models;


use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use xtribe\core\behaviors\TranslationBehavior;


class LaunchDate extends ActiveRecord
{
    
    public static function tableName()
    {
        return '{{launch_date}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => \xtribe\core\behaviors\TranslationBehavior::className(),
            ]
        ];
    }
}
